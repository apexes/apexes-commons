package net.apexes.commons.ormlite;

import com.j256.ormlite.field.DatabaseFieldConfig;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class ColumnDatabaseFieldConfig extends DatabaseFieldConfig {

    private int digits;

    ColumnDatabaseFieldConfig(String fieldName) {
        super(fieldName);
    }

    public int getDigits() {
        return digits;
    }

    public void setDigits(int digits) {
        this.digits = digits;
    }
}
