/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.ormlite;

import java.util.Collections;
import java.util.List;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class IndexImpl implements Index {

    private final String name;
    private final boolean unique;
    private final List<IndexColumn> columns;

    IndexImpl(String name, boolean unique, List<Index.IndexColumn> columns) {
        this.name = name;
        this.unique = unique;
        this.columns = Collections.unmodifiableList(columns);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public boolean isUnique() {
        return unique;
    }

    @Override
    public List<IndexColumn> columns() {
        return columns;
    }

}
