/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.ormlite;

import com.j256.ormlite.support.ConnectionSource;

import java.lang.reflect.Field;

/**
 *
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface UpgradeChecker {

    boolean exists(ConnectionSource connectionSource, Table<?> table) throws Exception;

    boolean exists(ConnectionSource connectionSource, Column column, Field field) throws Exception;
}
