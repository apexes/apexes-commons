/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.ormlite;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.support.DatabaseResults;
import net.apexes.commons.lang.Enume;

import java.sql.SQLException;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 * @param <E>
 */
public class EnumeByIntegerType<E extends Enume<Integer>> extends EnumeType<Integer, E> {
    
    public EnumeByIntegerType(Class<E> classType) {
        super(SqlType.INTEGER, classType);
    }
    
    @Override
    public Object parseDefaultString(FieldType fieldType, String defaultStr) throws SQLException {
        return Integer.valueOf(defaultStr);
    }

    @Override
    public Object resultToSqlArg(FieldType fieldType, DatabaseResults results, int columnPos) throws SQLException {
        return results.getInt(columnPos);
    }
    
    @Override
    public Object resultStringToJava(FieldType fieldType, String sqlArg, int columnPos) throws SQLException {
        return sqlArgToJava(fieldType, Integer.valueOf(sqlArg), columnPos);
    }
    
}
