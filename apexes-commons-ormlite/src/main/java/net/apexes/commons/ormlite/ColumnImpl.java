/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.ormlite;

import com.j256.ormlite.field.DatabaseFieldConfig;
import com.j256.ormlite.stmt.ColumnArg;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class ColumnImpl<T> extends ColumnArg implements Column {

    private final Table<T> table;
    private final DatabaseFieldConfig config;

    public ColumnImpl(Table<T> table, DatabaseFieldConfig config) {
        super(table.name(), config.getColumnName());
        this.table = table;
        this.config = config;
        this.table.addColumn(this);
    }

    @Override
    public String name() {
        return config.getColumnName();
    }

    @Override
    public DatabaseFieldConfig config() {
        return config;
    }

    @Override
    public Table<?> table() {
        return table;
    }
}
