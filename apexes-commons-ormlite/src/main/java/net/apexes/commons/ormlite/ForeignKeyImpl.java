/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.ormlite;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class ForeignKeyImpl implements ForeignKey {

    private final Column column;
    private final String referenceTable;
    private final String referenceColumn;

    ForeignKeyImpl(Column column, String referenceTable, String referenceColumn) {
        this.column = column;
        this.referenceTable = referenceTable;
        this.referenceColumn = referenceColumn;
    }

    @Override
    public Column column() {
        return column;
    }

    @Override
    public String referenceTable() {
        return referenceTable;
    }

    @Override
    public String referenceColumn() {
        return referenceColumn;
    }
}
