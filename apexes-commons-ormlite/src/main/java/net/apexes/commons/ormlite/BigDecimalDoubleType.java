/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *          http://www.apexes.net
 */

package net.apexes.commons.ormlite;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BaseDataType;
import com.j256.ormlite.misc.SqlExceptionUtil;
import com.j256.ormlite.support.DatabaseResults;
import net.apexes.commons.lang.Numbers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class BigDecimalDoubleType extends BaseDataType {

    private static final BigDecimalDoubleType singleTon = new BigDecimalDoubleType();

    public static BigDecimalDoubleType getSingleton() {
        return singleTon;
    }

    private BigDecimalDoubleType() {
        // this has no classes because {@link BigDecimalString} is the default
        super(SqlType.DOUBLE);
    }

    /**
     * Here for others to subclass.
     */
    protected BigDecimalDoubleType(SqlType sqlType, Class<?>[] classes) {
        super(sqlType, classes);
    }

    @Override
    public Object parseDefaultString(FieldType fieldType, String defaultStr) throws SQLException {
        try {
            return new BigDecimal(defaultStr);
        } catch (IllegalArgumentException e) {
            throw SqlExceptionUtil.create("Problems with field " + fieldType + " parsing default BigDecimal string '"
                    + defaultStr + "'", e);
        }
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
        if (javaObject == null) {
            return null;
        }
        try {
            BigDecimal value = (BigDecimal) javaObject;
            return convert(fieldType, value);
        } catch (Exception e) {
            throw SqlExceptionUtil.create("Problems with field " + fieldType + " javaToSqlArg '" + javaObject + "'", e);
        }
    }

    @Override
    public Object resultToSqlArg(FieldType fieldType, DatabaseResults results, int columnPos) throws SQLException {
        if (results.wasNull(columnPos)) {
            return null;
        }
        BigDecimal value = BigDecimal.valueOf(results.getDouble(columnPos));
        return convert(fieldType, value);
    }

    private BigDecimal convert(FieldType fieldType, BigDecimal value) {
        if (fieldType instanceof UnreflectFieldType) {
            UnreflectFieldType<?> type = (UnreflectFieldType<?>) fieldType;
            if (type.columnConfig != null) {
                int digits = type.columnConfig.getDigits();
                return Numbers.trimZero(value.setScale(digits, RoundingMode.HALF_UP));
            }
        }
        return Numbers.trimZero(value);
    }

    @Override
    public boolean isAppropriateId() {
        return false;
    }

    @Override
    public boolean isEscapedValue() {
        return false;
    }

    @Override
    public Class<?> getPrimaryClass() {
        return BigDecimal.class;
    }
}
