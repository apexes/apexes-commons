/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.ormlite;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface ForeignKey {

    Column column();

    String referenceTable();

    String referenceColumn();
}
