package net.apexes.commons.ormlite;

import com.j256.ormlite.field.DatabaseFieldConfig;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface ColumnFactory {

    <T> Column create(Table<T> table, DatabaseFieldConfig config);
}
