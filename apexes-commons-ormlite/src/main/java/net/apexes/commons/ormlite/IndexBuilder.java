/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.ormlite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class IndexBuilder {

    private final String name;
    private final List<Index.IndexColumn> columns;
    private boolean unique;

    IndexBuilder(String name) {
        this.name = name;
        this.columns = new ArrayList<>();
    }

    public IndexBuilder column(Column column) {
        return column(column, false);
    }

    public IndexBuilder column(Column column, boolean desc) {
        this.columns.add(new Index.IndexColumn(column, desc));
        return this;
    }

    public IndexBuilder unique() {
        this.unique = true;
        return this;
    }

    public final Index build() {
        return new IndexImpl(name, unique, columns);
    }
}
