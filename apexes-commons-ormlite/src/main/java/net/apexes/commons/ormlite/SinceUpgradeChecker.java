/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.ormlite;

import com.j256.ormlite.support.ConnectionSource;

import java.lang.reflect.Field;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class SinceUpgradeChecker implements UpgradeChecker {

    private final int oldVer;
    private final int newVer;

    public SinceUpgradeChecker(int oldVer, int newVer) {
        this.oldVer = oldVer;
        this.newVer = newVer;
    }

    @Override
    public boolean exists(ConnectionSource connectionSource, Table<?> table) {
        Class<?> tableClass = table.getClass();
        if (tableClass.isAnnotationPresent(Since.class)) {
            Since since = tableClass.getAnnotation(Since.class);
            int version = since.version();
            return oldVer < version && version <= newVer;
        }
        return true;
    }

    @Override
    public boolean exists(ConnectionSource connectionSource, Column column, Field field) {
        if (field.isAnnotationPresent(Since.class)) {
            Since since = field.getAnnotation(Since.class);
            int version = since.version();
            return oldVer < version && version <= newVer;
        }
        return true;
    }
}
