/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.ormlite;

import java.util.List;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface Index {

    String name();

    boolean isUnique();

    List<IndexColumn> columns();

    /**
     * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
     */
    class IndexColumn {

        private final Column column;
        private final boolean desc;

        IndexColumn(Column column, boolean desc) {
            this.column = column;
            this.desc = desc;
        }

        public Column getColumn() {
            return column;
        }

        public boolean isDesc() {
            return desc;
        }
    }
}
