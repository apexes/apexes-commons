/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.json.jackson;

import com.fasterxml.jackson.databind.JsonSerializer;
import net.apexes.commons.lang.Enume;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 * @param <E>
 */
public abstract class EnumeJsonSerializer<E extends Enume<? extends Comparable<?>>> extends JsonSerializer<E> {

    protected final Class<E> enumeClass;

    protected EnumeJsonSerializer(Class<E> enumeClass) {
        this.enumeClass = enumeClass;
    }

    @Override
    public Class<E> handledType() {
        return enumeClass;
    }
}
