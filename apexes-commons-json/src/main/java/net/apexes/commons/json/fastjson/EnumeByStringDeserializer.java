/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.json.fastjson;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import net.apexes.commons.lang.Enume;

import java.lang.reflect.Type;

/**
 *
 * @author hedyn
 * @param <E>
 */
public class EnumeByStringDeserializer<E extends Enume<String>> implements ObjectDeserializer {

    protected final Class<E> enumeClass;

    public EnumeByStringDeserializer(Class<E> enumeClass) {
        this.enumeClass = enumeClass;
    }

    @Override
    public E deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
        String value = parser.parseObject(String.class);
        return Enume.fromValue(enumeClass, value);
    }

    @Override
    public int getFastMatchToken() {
        return JSONToken.LITERAL_STRING;
    }
}
