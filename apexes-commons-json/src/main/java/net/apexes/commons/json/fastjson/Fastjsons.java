/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.json.fastjson;

import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.serializer.SerializeConfig;
import net.apexes.commons.lang.Enume;
import net.apexes.commons.lang.EnumeRegistry;

/**
 * @author hedyn
 */
public final class Fastjsons {
    private Fastjsons() {}

    public static void register(SerializeConfig serializeConfig, ParserConfig parserConfig, EnumeRegistry enumeRegistry) {
        for (Class<? extends Enume<?>> enumeClass : enumeRegistry.getEnumeClasses()) {
            register(serializeConfig, parserConfig, enumeClass);
        }
    }
    
    public static void register(SerializeConfig serializeConfig, ParserConfig parserConfig, Class<? extends Enume<?>> enumeClass) {
        Class<?> valueClass = Enume.valueClass(enumeClass);
        if (valueClass == String.class) {
            Class<Enume<String>> classType = (Class<Enume<String>>) enumeClass;
            serializeConfig.put(classType, new EnumeByStringSerializer<>());
            parserConfig.putDeserializer(classType, new EnumeByStringDeserializer(classType));
        } else if (valueClass == Integer.class) {
            Class<Enume<Integer>> classType = (Class<Enume<Integer>>) enumeClass;
            serializeConfig.put(classType, new EnumeByIntegerSerializer<>());
            parserConfig.putDeserializer(classType, new EnumeByIntegerDeserializer(classType));
        } else {
            throw new IllegalArgumentException("value only be String or Integer.");
        }
    }
}
