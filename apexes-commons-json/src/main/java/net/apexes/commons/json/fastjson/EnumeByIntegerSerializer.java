/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.json.fastjson;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import net.apexes.commons.lang.Enume;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 *
 * @author hedyn
 * @param <E>
 */
public class EnumeByIntegerSerializer<E extends Enume<Integer>> implements ObjectSerializer {

    @SuppressWarnings("unchecked")
    @Override
    public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features) throws IOException {
        Integer value;
        if (object == null) {
            value = null;
        } else {
            value = ((E) object).value();
        }
        serializer.write(value);
    }
}
