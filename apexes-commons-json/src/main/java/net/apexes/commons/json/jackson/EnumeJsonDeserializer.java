/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.json.jackson;

import com.fasterxml.jackson.databind.JsonDeserializer;
import net.apexes.commons.lang.Enume;

/**
 *
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 * @param <E>
 */
public abstract class EnumeJsonDeserializer<E extends Enume<? extends Comparable<?>>> extends JsonDeserializer<E> {

    protected final Class<E> enumeClass;

    protected EnumeJsonDeserializer(Class<E> enumeClass) {
        this.enumeClass = enumeClass;
    }
}
