/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.json.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import net.apexes.commons.lang.Enume;

import java.io.IOException;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 * @param <E>
 */
public class EnumeByStringJsonDeserializer<E extends Enume<String>> extends EnumeJsonDeserializer<E> {

    public EnumeByStringJsonDeserializer(Class<E> enumeClass) {
        super(enumeClass);
    }

    @Override
    public E deserialize(JsonParser jp, DeserializationContext context) throws IOException {
        return Enume.fromValue(enumeClass, jp.getText());
    }

}
