/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.json.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import net.apexes.commons.lang.Enume;

import java.io.IOException;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 * @param <E>
 */
public class EnumeByStringJsonSerializer<E extends Enume<String>> extends EnumeJsonSerializer<E> {
    
    public EnumeByStringJsonSerializer(Class<E> enumeClass) {
        super(enumeClass);
    }

    @Override
    public void serialize(E value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeString(Enume.toValue(value));
    }

}
