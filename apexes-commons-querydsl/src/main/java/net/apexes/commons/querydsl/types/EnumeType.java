/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl.types;

import com.querydsl.sql.types.Type;
import net.apexes.commons.lang.Enume;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public abstract class EnumeType<E extends Enume<? extends Comparable<?>>> implements Type<E> {

    private final Class<E> classType;
    private final int[] types;

    public EnumeType(Class<E> classType, int... types) {
        this.classType = classType;
        this.types = types;
    }

    @Override
    public final int[] getSQLTypes() {
        return types;
    }

    @Override
    public String getLiteral(E value) {
        return value.toString();
    }

    @Override
    public Class<E> getReturnedClass() {
        return classType;
    }
}
