/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl.info;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class PrimaryKeyInfo {
    
    private final String name;
    private final String schema;
    private final String table;
    private final List<String> columns;

    PrimaryKeyInfo(String name, String schema, String table) {
        this(name, schema, table, null);
    }

    PrimaryKeyInfo(String name, String schema, String table, String[] c) {
        this.name = name;
        this.schema = schema;
        this.table = table;
        columns = new ArrayList<>();
        if (c != null) {
            columns.addAll(Arrays.asList(c));
        }
    }

    public void add(String column) {
        columns.add(column);
    }

    public String getName() {
        return name;
    }

    public String getSchema() {
        return schema;
    }

    public String getTable() {
        return table;
    }

    public List<String> getColumns() {
        return columns;
    }

    public boolean isSameColumn(PrimaryKeyInfo pkInfo) {
        return TableInfo.isEquals(columns, pkInfo.getColumns());
    }

}
