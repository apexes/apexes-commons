/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl.info;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class IndexInfo {
    
    private final String name;
    private final String schema;
    private final String table;
    private final boolean unique;
    private final List<IndexColumn> columns;
    private final List<String> columnNames;
    private boolean primaryKeyIndex;
    private boolean foreignKeyIndex;

    public IndexInfo(String name, String schema, String table, boolean unique) {
        this.name = name;
        this.schema = schema;
        this.table = table;
        this.unique = unique;
        this.columns = new ArrayList<>();
        this.columnNames = new ArrayList<>();
    }

    public String getName() {
        return name;
    }
    
    public String getSchema() {
        return schema;
    }

    public String getTable() {
        return table;
    }
    
    public boolean isUnique() {
        return unique;
    }

    public List<IndexColumn> getColumns() {
        return columns;
    }

    public void addColumn(IndexColumn column) {
        columns.add(column);
        columnNames.add(column.getColumnName());
    }

    public boolean isKeyIndex() {
        return isPrimaryKeyIndex() || isForeignKeyIndex();
    }

    public boolean isPrimaryKeyIndex() {
        return primaryKeyIndex;
    }

    public void setPrimaryKeyIndex(boolean primaryKeyIndex) {
        this.primaryKeyIndex = primaryKeyIndex;
    }

    public boolean isForeignKeyIndex() {
        return foreignKeyIndex;
    }

    public void setForeignKeyIndex(boolean foreignKeyIndex) {
        this.foreignKeyIndex = foreignKeyIndex;
    }

    public boolean isSame(IndexInfo indexInfo) {
        return this.isUnique() == indexInfo.isUnique() && isSameColumn(indexInfo.columnNames);
    }

    public boolean isSameColumn(List<String> columns) {
        return TableInfo.isEquals(columnNames, columns);
    }

}
