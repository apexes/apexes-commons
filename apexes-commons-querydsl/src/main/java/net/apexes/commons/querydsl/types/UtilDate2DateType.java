/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl.types;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.querydsl.sql.types.AbstractDateTimeType;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class UtilDate2DateType extends AbstractDateTimeType<java.util.Date> {

    public UtilDate2DateType() {
        super(Types.DATE);
    }

    public UtilDate2DateType(int type) {
        super(type);
    }

    @Override
    public String getLiteral(java.util.Date value) {
        return dateFormatter.print(value.getTime());
    }

    @Override
    public java.util.Date getValue(ResultSet rs, int startIndex) throws SQLException {
        return rs.getDate(startIndex);
    }

    @Override
    public Class<java.util.Date> getReturnedClass() {
        return java.util.Date.class;
    }

    @Override
    public void setValue(PreparedStatement st, int startIndex, java.util.Date value) throws SQLException {
        st.setObject(startIndex, value);
    }

}
