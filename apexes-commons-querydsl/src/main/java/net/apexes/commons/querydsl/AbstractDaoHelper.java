/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl;

import com.querydsl.sql.SQLQueryFactory;
import net.apexes.commons.querydsl.sql.TablePathBase;

import javax.sql.DataSource;
import java.io.Serializable;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public abstract class AbstractDaoHelper implements DaoHelper {
    
    @Override
    public SQLQueryFactory factory() {
        return factory(true);
    }
    
    @Override
    public SQLQueryFactory factory(boolean release) {
        return getQuerydsl().createQueryFactory(getDataSource(), release);
    }
    
    @Override
    public <E, ID extends Serializable> Dao<E, ID> dao(TablePathBase<E> qvar) {
        return dao(qvar, true);
    }
    
    @Override
    public <E, ID extends Serializable> Dao<E, ID> dao(TablePathBase<E> qvar, boolean release) {
        return new QuerydslDao<>(factory(release), qvar);
    }

    @Override
    public <E, ID extends Serializable> Dao.UpdateExecuter<E, ID> update(TablePathBase<E> qvar) {
        Dao<E, ID> dao = dao(qvar);
        return dao.update();
    }

    protected abstract Querydsl getQuerydsl();
    
    protected abstract DataSource getDataSource();
}
