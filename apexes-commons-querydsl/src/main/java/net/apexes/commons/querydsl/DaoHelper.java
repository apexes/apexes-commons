/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl;

import com.querydsl.sql.SQLQueryFactory;
import net.apexes.commons.querydsl.sql.TablePathBase;

import java.io.Serializable;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public interface DaoHelper {
    
    /**
     * 创建一个{@link SQLQueryFactory}对象
     * @return 返回{@link SQLQueryFactory}对象
     */
    SQLQueryFactory factory();
    
    /**
     * 创建一个{@link SQLQueryFactory}对象
     * @param release 为true时close连接
     * @return 返回{@link SQLQueryFactory}对象
     */
    SQLQueryFactory factory(boolean release);
    
    /**
     * 创建一个Dao对象
     * @param qvar 要操作的表
     * @return 返回Dao对象
     */
    <E, ID extends Serializable> Dao<E, ID> dao(TablePathBase<E> qvar);
    
    /**
     * 创建一个Dao对象
     * @param qvar 要操作的表
     * @param release 为true时close连接
     * @return 返回Dao对象
     */
    <E, ID extends Serializable> Dao<E, ID> dao(TablePathBase<E> qvar, boolean release);

    <E, ID extends Serializable> Dao.UpdateExecuter<E, ID> update(TablePathBase<E> qvar);

}
