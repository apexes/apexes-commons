/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl;

import com.querydsl.sql.Configuration;
import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.types.Type;

import javax.sql.DataSource;
import java.util.Collection;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class QuerydslBase implements Querydsl {

    private final Configuration configuration;

    protected QuerydslBase(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public QuerydslBase register(Type<?> type) {
        configuration.register(type);
        return this;
    }
    
    @Override
    public QuerydslBase registerAll(Collection<Type<?>> types) {
        for (Type<?> type : types) {
            configuration.register(type);
        }
        return this;
    }
    
    @Override
    public Configuration configuration() {
        return configuration;
    }
    
    @Override
    public SQLQueryFactory createQueryFactory(ConnectionProvider provider) {
        return new SQLQueryFactory(configuration, provider);
    }
    
    @Override
    public SQLQueryFactory createQueryFactory(DataSource dataSource) {
        return createQueryFactory(dataSource, true);
    }
    
    @Override
    public SQLQueryFactory createQueryFactory(DataSource dataSource, boolean release) {
        return new SQLQueryFactory(configuration, dataSource, release);
    }

}
