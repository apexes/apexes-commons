/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl.info;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class ForeignKeyInfo {
    
    private final String name;
    private final String localSchema;
    private final String pkSchema;
    private final String localTable;
    private final String pkTable;
    private final List<String> localColumns = new ArrayList<>();
    private final List<String> pkColumns = new ArrayList<>();

    public ForeignKeyInfo(String name, String localTable, String pkTable) {
        this(name, null, localTable, null, pkTable);
    }
    
    public ForeignKeyInfo(String name, String localSchema, String localTable, String pkSchema, String pkTable) {
        this.name = name;
        this.localSchema = localSchema;
        this.pkSchema = pkSchema;
        this.localTable = localTable;
        this.pkTable = pkTable;
    }

    public void add(String localColumn, String pkColumn) {
        localColumns.add(localColumn);
        pkColumns.add(pkColumn);
    }

    public String getName() {
        return name;
    }
    
    public String getLocalSchema() {
        return localSchema;
    }

    public String getPkSchema() {
        return pkSchema;
    }

    public String getLocalTable() {
        return localTable;
    }

    public String getPkTable() {
        return pkTable;
    }

    public List<String> getLocalColumns() {
        return localColumns;
    }

    public List<String> getPkColumns() {
        return pkColumns;
    }

    public boolean isSameColumn(ForeignKeyInfo fkInfo) {
        return TableInfo.isEquals(localColumns, fkInfo.getLocalColumns())
                && TableInfo.isEquals(pkColumns, fkInfo.getPkColumns());
    }

}
