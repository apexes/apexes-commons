/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class Paging {

    /**
     * @param offset 开始记录序号
     * @param limit  限制条数
     * @return
     */
    public static Paging of(long offset, long limit) {
        return new Paging(offset, limit);
    }

    public static Paging first() {
        return of(0, 1);
    }

    public static Paging limit(long limit) {
        return new Paging(0, limit);
    }

    public static Paging offset(long offset) {
        return new Paging(offset, null);
    }

    private final long offset;
    private final Long limit;

    public Paging(long offset, Long limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public long getOffset() {
        return offset;
    }

    public Long getLimit() {
        return limit;
    }
}
