/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl.types;

import net.apexes.commons.lang.Enume;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 * @param <E>
 */
public class EnumeByStringValueType<E extends Enume<String>> extends EnumeType<E> {

    public EnumeByStringValueType(Class<E> classType) {
        this(classType, Types.CHAR, Types.VARCHAR);
    }

    public EnumeByStringValueType(Class<E> classType, int... jdbcTypes) {
        super(classType, jdbcTypes);
    }

    @Override
    public E getValue(ResultSet rs, int startIndex) throws SQLException {
        return Enume.fromValue(getReturnedClass(), rs.getString(startIndex));
    }

    @Override
    public void setValue(PreparedStatement st, int startIndex, E value) throws SQLException {
        st.setString(startIndex, Enume.toValue(value));
    }
    
}
