/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.Path;
import com.querydsl.sql.RelationalPath;

import javax.annotation.concurrent.Immutable;
import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
@Immutable
public class Index implements Serializable {

    private static final long serialVersionUID = 1L;

    private final RelationalPath<?> entity;

    private final String name;
    
    private final boolean unique;
    
    private final ImmutableList<IndexPath> indexPaths;
    
    public Index(RelationalPath<?> entity, String name, boolean unique, ImmutableList<IndexPath> indexPaths) {
        this.entity = entity;
        this.name = name;
        this.unique = unique;
        this.indexPaths = indexPaths;
    }

    public RelationalPath<?> getEntity() {
        return entity;
    }

    public String getName() {
        return name;
    }
    
    public boolean isUnique() {
        return unique;
    }

    public List<IndexPath> getPaths() {
        return indexPaths;
    }

    /**
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     */
    public static class IndexPath {

        private final Path<?> path;
        private final boolean desc;

        IndexPath(Path<?> path, boolean desc) {
            this.path = path;
            this.desc = desc;
        }

        public Path<?> getPath() {
            return path;
        }

        public boolean isDesc() {
            return desc;
        }
    }

}
