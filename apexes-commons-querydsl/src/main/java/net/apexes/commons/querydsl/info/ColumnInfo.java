/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl.info;

import java.sql.DatabaseMetaData;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class ColumnInfo {

    private final String name;
    private final String normalizedName;
    private final int jdbcType;
    private final String typeName;
    private final Integer size;
    private final Integer digits;
    private final int index;
    private final int nullable;
    private final String defaultValue;
    private final String describe;

    ColumnInfo(String name, String normalizedName, int jdbcType, String typeName, Integer size,
               Integer digits, int index, int nullable, String defaultValue, String describe) {
        this.name = name;
        this.normalizedName = normalizedName;
        this.jdbcType = jdbcType;
        this.typeName = typeName;
        this.size = size;
        this.digits = digits;
        this.index = index;
        this.nullable = nullable;
        this.defaultValue = defaultValue;
        this.describe = describe;
    }

    public String getName() {
        return name;
    }

    public String getNormalizedName() {
        return normalizedName;
    }

    public int getJdbcType() {
        return jdbcType;
    }

    public String getTypeName() {
        return typeName;
    }

    public Integer getSize() {
        return size;
    }

    public Integer getDigits() {
        return digits;
    }

    public int getIndex() {
        return index;
    }

    public int getNullable() {
        return nullable;
    }

    public boolean isNullable() {
        return !isNotNullable();
    }

    public boolean isNotNullable() {
        return nullable == DatabaseMetaData.columnNoNulls;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getDescribe() {
        return describe;
    }
}
