/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl.types;

import net.apexes.commons.lang.Enume;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 * @param <E>
 */
public class EnumeByIntegerValueType<E extends Enume<Integer>> extends EnumeType<E> {
    
    public EnumeByIntegerValueType(Class<E> classType) {
        this(classType, Types.INTEGER);
    }

    public EnumeByIntegerValueType(Class<E> classType, int... jdbcTypes) {
        super(classType, jdbcTypes);
    }
    
    @Override
    public E getValue(ResultSet rs, int startIndex) throws SQLException {
        return Enume.fromValue(getReturnedClass(), (Integer)rs.getObject(startIndex));
    }

    @Override
    public void setValue(PreparedStatement st, int startIndex, E value) throws SQLException {
        st.setObject(startIndex, Enume.toValue(value));
    }
    
}
