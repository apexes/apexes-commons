/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl;

import com.querydsl.sql.Configuration;
import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.SQLTemplates;
import com.querydsl.sql.types.Type;
import net.apexes.commons.querydsl.support.QuerydslEnumeSupport;

import javax.sql.DataSource;
import java.util.Collection;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public interface Querydsl {
    
    Querydsl register(Type<?> type);
    
    Querydsl registerAll(Collection<Type<?>> types);
    
    Configuration configuration();

    /**
     * 创建一个{@link SQLQueryFactory}对象
     * @param provider 数据库连接提供器
     * @return 返回{@link SQLQueryFactory}对象
     */
    SQLQueryFactory createQueryFactory(ConnectionProvider provider);

    /**
     * 创建一个{@link SQLQueryFactory}对象
     * @param dataSource 数据库连接
     * @return 返回{@link SQLQueryFactory}对象
     */
    SQLQueryFactory createQueryFactory(DataSource dataSource);
    
    /**
     * 创建一个{@link SQLQueryFactory}对象
     * @param dataSource 数据库连接
     * @param release 为true时close连接
     * @return 返回{@link SQLQueryFactory}对象
     */
    SQLQueryFactory createQueryFactory(DataSource dataSource, boolean release);

    static Querydsl of(SQLTemplates sqlTemplates) {
        return new QuerydslBase(new Configuration(sqlTemplates));
    }
    
    static Querydsl of(Configuration configuration) {
        return new QuerydslBase(configuration);
    }
    
    static Querydsl ofSimple(SQLTemplates sqlTemplates) {
        return new QuerydslBase(QuerydslEnumeSupport.simpleConfiguration(sqlTemplates));
    }
}
