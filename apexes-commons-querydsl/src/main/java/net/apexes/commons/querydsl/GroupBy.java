/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class GroupBy {

    public static GroupBy of(Path<?>... columns) {
        return new GroupBy(columns);
    }

    private final Path<?>[] columns;
    private Predicate[] havings;

    public GroupBy(Path<?>... columns) {
        this.columns = columns;
    }

    public GroupBy having(Predicate... havings) {
        this.havings = havings;
        return this;
    }

    public Path<?>[] getColumns() {
        return columns;
    }

    public Predicate[] getHavings() {
        return havings;
    }
}
