/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl;

import com.querydsl.core.types.Path;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * @param <T>
 * @param <ID>
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class PathValuePair<T, ID extends Serializable> {

    private final Path<?>[] paths;
    private final Object[] values;

    public PathValuePair(List<Path<?>> pathList, List<?> valueList) {
        this.paths = pathList.toArray(new Path<?>[0]);
        this.values = valueList.toArray();
    }

    public PathValuePair(QuerydslHelper<T, ID> helper, T entity, Path<?>[] paths) {
        this.paths = new Path<?>[paths.length];
        this.values = new Object[paths.length];
        for (int i = 0; i < paths.length; i++) {
            this.paths[i] = paths[i];
            this.values[i] = helper.getValue(entity, paths[i]);
        }
    }

    public Path<?>[] getPaths() {
        return paths;
    }

    public Object[] getValues() {
        return values;
    }

    public List<Path<?>> getPathsAsList() {
        return Arrays.asList(paths);
    }

    public List<?> getValuesAsList() {
        return Arrays.asList(values);
    }

    public int getSize() {
        return paths.length;
    }

    public Path<?> getPath(int index) {
        return paths[index];
    }

    public Object getValue(int index) {
        return values[index];
    }

    @Override
    public String toString() {
        return super.toString()
                + "#[paths=" + Arrays.toString(paths) + ", values=" + Arrays.toString(values) + "]";
    }

}
