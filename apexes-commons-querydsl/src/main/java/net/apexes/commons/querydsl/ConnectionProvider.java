/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.querydsl;

import java.sql.Connection;

import javax.inject.Provider;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class ConnectionProvider implements Provider<Connection> {
    
    private final Connection conn;
    
    private ConnectionProvider(Connection conn) {
        this.conn = conn;
    }

    @Override
    public Connection get() {
        return conn;
    }
    
    public static ConnectionProvider of(Connection conn) {
        return new ConnectionProvider(conn);
    }

}
