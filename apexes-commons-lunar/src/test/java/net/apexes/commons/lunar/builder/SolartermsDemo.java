/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lunar.builder;

import java.util.Date;

import net.apexes.commons.lunar.Solarterms;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class SolartermsDemo {
		
	public static void main(String[] args) throws Exception {
		java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
		java.text.SimpleDateFormat df1 = new java.text.SimpleDateFormat("E");
		System.out.println(df1.format(new Date()));
		int index;
		Date date = Solarterms.getSolartermDate(2015, 9);
		System.out.println(df.format(date));
		index = Solarterms.getSolartermIndex(date);
		System.out.println("节气：" + index);
		
		date = df.parse("2015-05-22 00:00");
		index = Solarterms.getSolartermIndex(date);
		System.out.println("节气：" + index);
		
		date = df.parse("2015-04-05 00:00");
		index = Solarterms.getSolartermIndex(date);
		System.out.println("节气：" + index);
		
		date = Solarterms.getSolartermDate(2015, index);
		System.out.println(df.format(date));
	}

}
