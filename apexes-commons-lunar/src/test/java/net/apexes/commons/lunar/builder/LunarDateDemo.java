/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lunar.builder;

import java.util.Date;

import net.apexes.commons.lunar.LunarDate;
import net.apexes.commons.lunar.LunarFormaters;

/**
 *
 * @author HeDYn<hedyn@foxmail.com>
 */
public class LunarDateDemo {

	public static void main(String[] args) throws Exception {
		java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd");
		java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy年MM月dd日");
		System.out.printf("农历范围：%d年至%d年\n", LunarDate.supportMinLunarYear(), LunarDate.supportMaxLunarYear());
		System.out.printf("公历范围：%s至%s\n", format.format(LunarDate.supportStratDate()),
				format.format(LunarDate.supportEndDate()));

		Date date;
		LunarDate lunarDate;
		date = new java.util.Date();
		lunarDate = LunarDate.valueOf(date);
		System.out.printf("%s:%s  %s\n", format.format(LunarDate.toDate(lunarDate)),
                LunarFormaters.formatFullDate(lunarDate), LunarFormaters.formatSolarterm(date));

		date = df.parse("2015-05-21");
		lunarDate = LunarDate.valueOf(date);
		System.out.printf("%s:%s  %s\n", format.format(LunarDate.toDate(lunarDate)),
				LunarFormaters.formatFullDate(lunarDate), LunarFormaters.formatSolarterm(date));
		
		date = df.parse("2006-08-24");
		lunarDate = LunarDate.valueOf(date);
		System.out.printf("%s:%s  %s\n", format.format(LunarDate.toDate(lunarDate)),
                LunarFormaters.formatFullDate(lunarDate), LunarFormaters.formatSolarterm(date));

		date = df.parse("2006-09-02");
		lunarDate = LunarDate.valueOf(date);
		System.out.printf("%s:%s  %s\n", format.format(LunarDate.toDate(lunarDate)),
                LunarFormaters.formatFullDate(lunarDate), LunarFormaters.formatSolarterm(date));

		date = df.parse("2008-10-29");
		lunarDate = LunarDate.valueOf(date);
		System.out.printf("%s:%s  %s\n", format.format(LunarDate.toDate(lunarDate)),
				LunarFormaters.formatFullDate(lunarDate), LunarFormaters.formatSolarterm(date));

		date = df.parse("2011-01-11");
		lunarDate = LunarDate.valueOf(date);
		System.out.printf("%s:%s  %s\n", format.format(LunarDate.toDate(lunarDate)),
                LunarFormaters.formatFullDate(lunarDate), LunarFormaters.formatSolarterm(date));

		date = df.parse("2011-02-01");
		lunarDate = LunarDate.valueOf(date);
		System.out.printf("%s:%s  %s\n", format.format(LunarDate.toDate(lunarDate)),
                LunarFormaters.formatFullDate(lunarDate), LunarFormaters.formatSolarterm(date));

		date = df.parse("2136-01-29");
		lunarDate = LunarDate.valueOf(date);
		System.out.printf("%s:%s  %s\n", format.format(LunarDate.toDate(lunarDate)),
                LunarFormaters.formatFullDate(lunarDate), LunarFormaters.formatSolarterm(date));

		date = df.parse("2136-01-30");
		lunarDate = LunarDate.valueOf(date);
		System.out.printf("%s:%s  %s\n", format.format(LunarDate.toDate(lunarDate)),
                LunarFormaters.formatFullDate(lunarDate), LunarFormaters.formatSolarterm(date));

		date = df.parse("2136-01-31");
		lunarDate = LunarDate.valueOf(date);
		System.out.printf("%s:%s  %s\n", format.format(LunarDate.toDate(lunarDate)),
                LunarFormaters.formatFullDate(lunarDate), LunarFormaters.formatSolarterm(date));

		date = df.parse("2136-02-01");
		lunarDate = LunarDate.valueOf(date);
		System.out.printf("%s:%s  %s\n", format.format(LunarDate.toDate(lunarDate)),
                LunarFormaters.formatFullDate(lunarDate), LunarFormaters.formatSolarterm(date));
	}

}
