/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lunar.builder;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import edu.npu.fastexcel.BIFFSetting;
import edu.npu.fastexcel.FastExcel;
import edu.npu.fastexcel.Sheet;
import edu.npu.fastexcel.Workbook;

/**
 *
 * @author HeDYn<hedyn@foxmail.com>
 */
public class LunarBuilder {
	
	private static final int START_YEAR = 1583;
	private static final int YEAR_COUNT = 553;
	
	// ---- ---- -FFF FFFM MMMM MMMM MMMm LLLL
	// F: 春节离元旦的天数
	// M: 每位表示该年一月至十二月的天数，为1表示30天，为0表示29天，一月在最高位；
	
    /**
     * MMMM BFFF FF_d DDDD DDDD DDDD
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
    	File dir = new File("src/main/resources/net/apexes/lunar");
        File file = new File(dir, "lunar.dat");
        DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
        out.writeInt(START_YEAR);
        out.writeInt(YEAR_COUNT);
        Workbook workbook;
        workbook = FastExcel.createReadableWorkbook(new File("lunar.xls"));
        workbook.setSSTType(BIFFSetting.SST_TYPE_DEFAULT);
        workbook.open();
        Sheet sheet;
        sheet = workbook.getSheet(0);
        for (int i = 0; i < YEAR_COUNT; i++) {
        	int row = i + 1;
            String s = sheet.getCell(row, 0);
            int year = Integer.valueOf(s.substring(0, 4)).intValue();
            if (year % 10 == 0) {
                System.out.println(" //" + (year - 10) + "-" + (year - 1));
            }
            int sfDays = Integer.valueOf(s.substring(8));
            String sMonth = s.substring(5, 7);
            if ("02".equals(sMonth)) {
            	sfDays += 31;
            }
            if (year == 1982) {
            	System.out.println(s + " : " + sfDays);
            }
            int monthDays = 0;
            for (int j = 1; j <= 12; j++) {
                s = sheet.getCell(row, j);
                if ("30".equals(s)) {
                    monthDays |= (1 << (12 - j));
                }
                if (year == 1982) {
                	System.out.print(s + " ");
                }
            }
            if (year == 1982) {
            	System.out.println(" " + Integer.toBinaryString(monthDays));
            }
            s = sheet.getCell(row, 13);
            int leapMonth = 0;
            int leapDays  = 0;
            if (!s.isEmpty()) {
                leapMonth  = Integer.valueOf(s).intValue();
                s = sheet.getCell(row, 14);
                if ("30".equals(s)) {
                    leapDays = 1;
                }
            }
            // ---- ---- -FFF FFFM MMMM MMMM MMMm LLLL
            int value = (sfDays << 17) | (monthDays << 5 ) | (leapDays << 4) | leapMonth;
            out.writeInt(value);
            System.out.print("0x" + Integer.toHexString(value) + ", ");
        }
        workbook.close();
        out.flush();
        out.close();
        System.out.println();
        System.out.println("------------------------------");
        DataInputStream in = new DataInputStream(new FileInputStream(file));
        int i = in.readInt();
        in.readInt();
        int value = -1;
        try {
            while ((value=in.readInt()) != -1) {
                if (i % 10 == 0) {
                    System.out.println(" //" + (i - 10) + "-" + (i - 1));
                }
                i++;
                System.out.print("0x" + Integer.toHexString(value) + ", ");
            }
        } catch (Exception e) {
            
        } finally {
            in.close();
        }
    }
}
