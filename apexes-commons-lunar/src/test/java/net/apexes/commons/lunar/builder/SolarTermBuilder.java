/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lunar.builder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import edu.npu.fastexcel.BIFFSetting;
import edu.npu.fastexcel.FastExcel;
import edu.npu.fastexcel.Sheet;
import edu.npu.fastexcel.Workbook;

/**
 *
 * @author HeDYn<hedyn@foxmail.com>
 */
public class SolarTermBuilder {
	
	private static final int START_YEAR = 1583;
	private static final int YEAR_COUNT = 553;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception  {
    	File dir = new File("src/main/resources/net/apexes/lunar");
        File file = new File(dir, "solarterms.dat");
        DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
        out.writeInt(START_YEAR);
        out.writeInt(YEAR_COUNT);
        Workbook workbook;
        workbook = FastExcel.createReadableWorkbook(new File("lunar.xls"));
        workbook.setSSTType(BIFFSetting.SST_TYPE_DEFAULT);
        workbook.open();
        Sheet sheet;
        sheet = workbook.getSheet(0);
        for (int i = 1; i <= YEAR_COUNT; i++) {
            for (int j = 15; j < 39; j+=2) {
                String s = sheet.getCell(i, j);
                int d = Integer.valueOf(s.substring(8, 10)).intValue();
                int h = Integer.valueOf(s.substring(11, 13)).intValue();
                int m = Integer.valueOf(s.substring(14, 16)).intValue();
                int v = 0;
                // dddd dhhh hhmm mmmm
                v = (d << 27) | (h << 22) | (m << 16);

                if (i + START_YEAR == 2016) {
                	
                	System.out.println("#" + i + ": " + s + ": " + d + " : " + h + " : " + m + "  # " + Integer.toBinaryString(v));
                }
                
                s = sheet.getCell(i, j+1);
                d = Integer.valueOf(s.substring(8, 10)).intValue();
                h = Integer.valueOf(s.substring(11, 13)).intValue();
                m = Integer.valueOf(s.substring(14, 16)).intValue();
                v |= (d << 11) | (h << 6) | m;
                System.out.print(Integer.toHexString(v) + "|" 
                        + ((v>>27)&0x1f) + "-" + ((v>>22)&0x1f) + ":"+((v>>16)&0x3f)+"|"
                        + ((v>>11)&0x1f) + "-" + ((v>>6)&0x1f) + ":"+(v & 0x3f) + " ");
                out.writeInt(v);
            }
            System.out.println();
        }
        workbook.close();
        out.flush();
        out.close();
    }
    
}
