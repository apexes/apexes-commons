/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lunar;

import java.io.DataInputStream;
import java.io.InputStream;

/**
 *
 * solarterms.dat 数据格式：
 * <pre>
 * offset   size    content
 * 0        4       开始年份
 * 4        4       年数
 * 8        4*N     节气信息数据
 *
 * 节气信息数据
 * 每个节气年占16位，两个节气占用一个int值，一个节气的数据格式：
 * DDDD DHHH HHMM MMMM
 *
 * D    5位，节气所在的日期
 * H    5位，节气所在的小时
 * M    6位，节气所在的分钟
 *
 * </pre>
 *
 * @author HeDYn<hedyn@foxmail.com>
 */
class Solartermsdat {

    static final Solartermsdat INSTANCE = new Solartermsdat();

    private int[] datas;
    private int startYear;

    private Solartermsdat() {
        InputStream is = getClass().getResourceAsStream("solarterms.dat");
        DataInputStream dis = new DataInputStream(is);
        try {
            startYear = dis.readInt();
            int len = dis.readInt();
            len *= 12;
            datas = new int[len];
            for (int i = 0; i < len; i++) {
                datas[i] = dis.readInt();
            }
        } catch (Exception e) {
            throw new IllegalStateException("Loading solarterms.dat error.");
        } finally {
            try {
                dis.close();
            } catch (Exception e) {
            }
        }
    }

    Integer data(int year, int month) {
        int i = (year - startYear) * 12;
        if (i < 0 || i >= datas.length) {
            return null;
        }
        return datas[i + month];
    }
}
