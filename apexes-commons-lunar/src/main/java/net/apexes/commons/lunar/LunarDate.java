/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lunar;

import java.util.Calendar;
import java.util.Date;

/**
 * 农历日期
 * 
 * @author HeDYn<hedyn@foxmail.com>
 */
public final class LunarDate implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int year;
	/**
	 * 阴历月份，从1开始
	 */
	private int month;
	/**
	 * 阴历日期，从1开始
	 */
	private int day;
	/**
	 * 是否为闰月
	 */
	private boolean leapMonth;

	LunarDate(int year, int month, int day, boolean leapMonth) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.leapMonth = leapMonth;
	}

	/**
	 * 返回阴历年份
	 * @return
	 */
	public int getYear() {
		return year;
	}

	/**
	 * 返回阴历月份，从1开始
	 * @return
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * 返回阴历日期，从1开始
	 * @return
	 */
	public int getDay() {
		return day;
	}

	/**
	 * 当前阴历月份是否是闰月，返回true表示当前阴历月份为闰月
	 * @return
	 */
	public boolean isLeapMonth() {
		return leapMonth;
	}

	/**
	 * 返回天干序号。天干数组为：
	 * "甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸"
	 * @return
	 */
	public int getHeavenlystemIndex() {
		return (year + 6) % 10;
	}

	/**
	 * 返回地支序号。地支数组为：
	 * "子(鼠)", "丑(牛)", "寅(虎)", "卯(兔)", "辰(龙)", "巳(蛇)",
	 * "午(马)", "未(羊)", "申(猴)", "酉(鸡)", "戌(狗)", "亥(猪)"
	 * @return
	 */
	public int getEarthlybranchIndex() {
		return (year + 8) % 12;
	}

	@Override
	public String toString() {
		return "LunarDate{" + "year=" + year 
				+ ", month=" + month 
				+ ", day=" + day 
				+ ", leapMonth=" + leapMonth + '}';
	}

	/**
	 * 支持的最小阴历年份
	 * @return
	 */
	public static int supportMinLunarYear() {
		return Lunardat.INSTANCE.getStartLunarYear();
	}

	/**
	 * 支持的最大阴历年份
	 * @return
	 */
	public static int supportMaxLunarYear() {
		return Lunardat.INSTANCE.getEndLunarYear();
	}

	/**
	 * 支持的最小公历日期
	 * @return
	 */
	public static Date supportStratDate() {
		return Lunardat.INSTANCE.getStratDate();
	}

	/**
	 * 支持的最大公历日期
	 * @return
	 */
	public static Date supportEndDate() {
		return Lunardat.INSTANCE.getEndDate();
	}

	/**
	 * 公历日期转为阴历日期
	 * @param date
	 * @return
	 */
	public static LunarDate valueOf(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return Lunardat.INSTANCE.convertToLunarDate(calendar);
	}

	/**
	 * 阴历日期转为公历日期
	 * @param date
	 * @return
	 */
	public static Date toDate(LunarDate date) {
		Calendar calendar = Lunardat.INSTANCE.convertToCalendar(date);
		return calendar.getTime();
	}

}
