/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lunar;

import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class LunarFormaters {
	
	public static String formatFullDate(LunarDate lunarDate) {
		return formatYear(lunarDate) + formatMonth(lunarDate) + formatDay(lunarDate);
	}
	
	public static String formatYear(LunarDate lunarDate) {
		int year = lunarDate.getYear();
		int i = (year + 6) % 10;
		int j = (year + 8) % 12;
		return HEAVENLY_STEM[i] + EARTHLY_BRANCH[j] + "年";
	}
	
	public static String formatMonth(LunarDate lunarDate) {
		return (lunarDate.isLeapMonth() ? "闰" : "") + MONTH_NAME[lunarDate.getMonth() - 1];
	}
	
	public static String formatDay(LunarDate lunarDate) {
		return DAY_NAME[lunarDate.getDay() - 1];
	}
	
	public static String formatMonthDay(LunarDate lunarDate) {
	    return formatMonth(lunarDate) + formatDay(lunarDate);
	}
	
	public static String formatSolarterm(Date date) {
		int index = Solarterms.getSolartermIndex(date);
		if (index != -1) {
			return SOLAR_TERM_NAME[index];
		}
		return "";
	}
	
	public static String formatSolarterm(Date date, SolartermFormat fmt) {
	    int index = Solarterms.getSolartermIndex(date);
        if (index != -1) {
            String solartermName = SOLAR_TERM_NAME[index];
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int year = calendar.get(Calendar.YEAR);
            Date solartermDate = Solarterms.getSolartermDate(year, index);
            return fmt.format(solartermName, solartermDate);
        }
        return "";
    }
	
	private static final String HEAVENLY_STEM[] = {
        "甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸"
    };

    private static final String EARTHLY_BRANCH[] = {
        "子(鼠)", "丑(牛)", "寅(虎)", "卯(兔)", "辰(龙)", "巳(蛇)",
        "午(马)", "未(羊)", "申(猴)", "酉(鸡)", "戌(狗)", "亥(猪)"
    };

    private static final String MONTH_NAME[] = {
        "正月", "二月", "三月", "四月", "五月", "六月",
        "七月", "八月", "九月", "十月", "十一月", "十二月"
    };

    private static final String DAY_NAME[] = {
        "初一", "初二", "初三", "初四", "初五",
        "初六", "初七", "初八", "初九", "初十",
        "十一", "十二", "十三", "十四", "十五",
        "十六", "十七", "十八", "十九", "二十",
        "廿一", "廿二", "廿三", "廿四", "廿五",
        "廿六", "廿七", "廿八", "廿九", "三十"
    };

    private static final String SOLAR_TERM_NAME[] = {
        "小寒", "大寒", "立春", "雨水", "惊蛰", "春分",
        "清明", "谷雨", "立夏", "小满", "芒种", "夏至",
        "小暑", "大暑", "立秋", "处暑", "白露", "秋分",
        "寒露", "霜降", "立冬", "小雪", "大雪", "冬至"
    };
    
    /**
     * 
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     *
     */
    public interface SolartermFormat {
        String format(String solartermName, Date date);
    }

}
