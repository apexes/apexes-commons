/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.xml;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class Xml extends XmlElement {

    public Xml(String name) {
        setName(name);
    }
    
    public Xml setValue(String content) {
        setContent(content);
        return this;
    }
    
    public Xml setAttr(String name, Object value) {
        setAttribute(name, value);
        return this;
    }
    
    public Xml addChild(String childName) {
        return addChild(new Xml(childName));
    }
    
    public Xml addChild(Xml child) {
        super.addChild(child);
        return this;
    }
    
    public Xml addChild(String name, String value) {
        return addChild(Xml.element(name, value));
    }
    
    public static Xml name(String name) {
        return new Xml(name);
    }
    
    public static Xml element(String name, String value) {
        return new Xml(name).setValue(value);
    }

}
