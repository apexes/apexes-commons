/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.i18n;

import net.apexes.commons.lang.Checks;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class I18Ns {

    private static final Map<String, String> CURRENCY_SYMBOL = new HashMap<>();
    static {
        CURRENCY_SYMBOL.put("CNY", "¥");//人民币
        CURRENCY_SYMBOL.put("EUR", "€");//欧元
        CURRENCY_SYMBOL.put("GBP", "£");//英镑
        CURRENCY_SYMBOL.put("JPY", "¥");//日元
        CURRENCY_SYMBOL.put("KRW", "₩");//韩元
        CURRENCY_SYMBOL.put("VND", "₫");//越南盾
        CURRENCY_SYMBOL.put("LAK", "₭");//老挝基普
        CURRENCY_SYMBOL.put("PHP", "₱");//菲律宾比索
        CURRENCY_SYMBOL.put("THB", "฿");//泰铢
        CURRENCY_SYMBOL.put("KHR", "៛");//柬埔寨瑞尔
    }
    
    public static String currencySymbol(String currencyCode) {
        String symbol = CURRENCY_SYMBOL.get(currencyCode);
        return symbol == null ? "$" : symbol;
    }

    public static Locale toLocale(String localeStr) {
        Checks.verifyNotEmpty(localeStr, "localeStr");
        int idx = localeStr.indexOf('_');
        if (idx == -1) {
            return new Locale(localeStr);
        }
        String language = localeStr.substring(0, idx);
        String country = localeStr.substring(idx + 1);
        return new Locale(language.toLowerCase(), country.toUpperCase());
    }

    public static I18nMsg msg(String bundleName, Locale locale) {
        return new I18nMsg(i18nResource(bundleName, locale));
    }

    public static I18nMsg msg(String bundleName, Locale locale, I18nMsg.Logger logger) {
        return new I18nMsg(i18nResource(bundleName, locale), logger);
    }

    public static EnumMsg enumMsg(String bundleName, Locale locale) {
        return new EnumMsg(i18nResource(bundleName, locale));
    }

    public static EnumMsg enumMsg(String bundleName, Locale locale, I18nMsg.Logger logger) {
        return new EnumMsg(i18nResource(bundleName, locale), logger);
    }

    public static I18nResource i18nResource(String bundleName, Locale locale) {
        return new JdkI18nResource(bundleName, locale);
    }

}
