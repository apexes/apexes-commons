package net.apexes.commons.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author hedyn
 */
public class JdkI18nResource implements I18nResource {

    private ResourceBundle resourceBundle;

    public JdkI18nResource(String bundleName, Locale locale) {
        if (locale == null) {
            locale = Locale.getDefault();
        }
        try {
            this.resourceBundle = ResourceBundle.getBundle(bundleName, locale);
        } catch (Exception e) {
            this.resourceBundle = ResourceBundle.getBundle(bundleName);
        }
    }

    @Override
    public Locale getLocale() {
        return resourceBundle.getLocale();
    }

    @Override
    public boolean containsKey(String key) {
        return resourceBundle.containsKey(key);
    }

    @Override
    public String getString(String key) {
        return resourceBundle.getString(key);
    }
}
