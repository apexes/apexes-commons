/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.i18n;

import java.util.Locale;

/**
 *
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface I18nResource {

    Locale getLocale();

    boolean containsKey(String key);

    String getString(String key);

}
