/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.i18n;

import net.apexes.commons.lang.Enume;

import java.util.Locale;

/**
 * 
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 *
 */
public final class EnumMsg {
    
    private final I18nMsg msg;

    public EnumMsg(I18nResource resource) {
        msg = new I18nMsg(resource);
    }

    public EnumMsg(I18nResource resource, I18nMsg.Logger logger) {
        msg = new I18nMsg(resource, logger);
    }
    
    public Locale getLocale() {
        return msg.getLocale();
    }
    
    /**
     * 返回枚举的名称
     * @param theEnum
     * @return
     */
    public String getEnumName(Enum<?> theEnum) {
        if (theEnum == null) {
            return null;
        }
        return getEnumName(theEnum, theEnum.name());
    }
    
    public String getEnumName(Enum<?> theEnum, String defaultValue) {
        Class<?> enumClass = theEnum.getClass();
        String key;
        if (enumClass.isMemberClass()) {
            key = enumClass.getDeclaringClass().getSimpleName()
                    + "." + enumClass.getSimpleName() 
                    + "." + theEnum.name();
        } else {
            key = enumClass.getSimpleName() + "." + theEnum.name();
        }
        return msg.getMessage(key, defaultValue);
    }
    
    /**
     * 返回Enume的名称。类内部定义的枚举则key中包含该类的名称
     * @param theEnume
     * @return
     */
    public <T extends Enume<?>> String getEnumName(T theEnume) {
        if (theEnume == null) {
            return null;
        }
        return getEnumName(theEnume, enumNameOrValue(theEnume));
    }
    
    public <T extends Enume<?>> String getEnumName(T theEnume, String defaultValue) {
        Class<?> enumClass = theEnume.getClass();
        String enumeName = enumNameOrValue(theEnume);
        String key;
        if (enumClass.isMemberClass()) {
            key = enumClass.getDeclaringClass().getSimpleName()
                    + "." + enumClass.getSimpleName() 
                    + "." + enumeName;
        } else {
            key = enumClass.getSimpleName() + "." + enumeName;
        }
        return msg.getMessage(key, defaultValue);
    }
    
    private <T extends Enume<?>> String enumNameOrValue(T theEnume) {
        String enumeName = theEnume.name();
        if (enumeName == null) {
            enumeName = String.valueOf(theEnume.value());
        }
        return enumeName;
    }
    
}
