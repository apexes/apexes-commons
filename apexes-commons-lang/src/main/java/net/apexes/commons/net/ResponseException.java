/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.net;

import net.apexes.commons.lang.Checks;

import java.net.SocketException;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class ResponseException extends SocketException {

    private static final long serialVersionUID = 1L;
    private final int responseCode;
    private final String responseMessage;
    private final String errorMessage;

    public ResponseException(int responseCode, String responseMessage) {
        this(responseCode, responseMessage, null);
    }

    public ResponseException(int responseCode, String responseMessage, String errorMessage) {
        super(message(responseCode, responseMessage, errorMessage));
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.errorMessage = errorMessage;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    private static String message(int responseCode, String responseMessage, String errorMessage) {
        StringBuilder message = new StringBuilder()
                .append(responseCode).append(" ")
                .append(responseMessage);
        if (Checks.isNotEmpty(errorMessage)) {
            String lineSeparator = System.getProperty("line.separator");
            message.append(lineSeparator).append(errorMessage);
        }
        return message.toString();
    }
}
