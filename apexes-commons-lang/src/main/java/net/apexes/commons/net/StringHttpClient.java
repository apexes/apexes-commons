/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.net;

import net.apexes.commons.lang.Checks;
import net.apexes.commons.lang.Networks;
import net.apexes.commons.lang.Streams;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class StringHttpClient {

    public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
    public static final String CONTENT_TYPE_APPLICATION_OCTETSTREAM = "application/octet-stream";

    public static final String METHOD_POST = "POST";
    public static final String METHOD_GET = "GET";
    public static final String PROPERTY_CONTENT_ENCODING = "Content-Encoding";
    public static final String PROPERTY_ACCEPT_ENCODING = "Accept-Encoding";
    public static final String PROPERTY_CONNECTION = "Connection";
    public static final String PROPERTY_CONTENT_TYPE = "Content-Type";
    public static final String PROPERTY_CONTENT_LEN = "Content-Length";
    public static final String CONNECTION_KEEP_ALIVE = "Keep-Alive";

    public static final String GZIP = "gzip";

    private static final RequestEncoder DEFAULT_REQUEST_ENCODER = new SimpleRequestEncoder();

    private final String url;
    private final Map<String, String> httpProperties;
    private final String contentType;
    private String charset;
    private int connectTimeout = 3 * 1000;
    private int readTimeout = 30 * 1000;
    private boolean acceptCompress;
    private SSLContext sslContext;
    private HostnameVerifier hostNameVerifier;
    private RequestLogger sentLogger;
    private RequestLogger requestLogger;
    private ResponseLogger responseLogger;
    private RequestEncoder requestEncoder;
    private ResponseReader responseReader;

    public StringHttpClient(String url) {
        this(url, CONTENT_TYPE_APPLICATION_JSON);
    }

    public StringHttpClient(String url, String contentType) {
        Checks.verifyNotNull(url, "url");
        Checks.verifyNotNull(contentType, "contentType");
        this.url = url;
        this.contentType = contentType;
        httpProperties = new LinkedHashMap<>();
    }

    public String getCharset() {
        if (charset == null) {
            return "UTF-8";
        }
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public boolean isAcceptCompress() {
        return acceptCompress;
    }

    public StringHttpClient setAcceptCompress(boolean value) {
        acceptCompress = value;
        return this;
    }
    
    /**
     * 设置连接超时时间
     * @param timeout 超时时间，单位ms
     */
    public StringHttpClient setConnectTimeout(int timeout) {
        this.connectTimeout = timeout;
        return this;
    }
    
    /**
     * 设置读取超时时间
     * @param timeout 超时时间，单位ms
     */
    public StringHttpClient setReadTimeout(int timeout) {
        this.readTimeout = timeout;
        return this;
    }
    
    public StringHttpClient setSslContext(SSLContext sslContext) {
        this.sslContext = sslContext;
        return this;
    }
    
    public StringHttpClient setHostNameVerifier(HostnameVerifier hostNameVerifier) {
        this.hostNameVerifier = hostNameVerifier;
        return this;
    }

    public StringHttpClient exemptSSL() throws KeyManagementException, NoSuchAlgorithmException {
        this.sslContext = Networks.exemptSSLContext();
        this.hostNameVerifier = Networks.exemptHostnameVerifier();
        return this;
    }

    public StringHttpClient setHttpProperty(String key, String value) {
        Checks.verifyNotNull(key, "key");
        Checks.verifyNotNull(value, "value");
        httpProperties.put(key, value);
        return this;
    }

    public String getHttpProperty(String key) {
        Checks.verifyNotNull(key, "key");
        return httpProperties.get(key);
    }

    public StringHttpClient setSentLogger(RequestLogger sentLogger) {
        this.sentLogger = sentLogger;
        return this;
    }
    
    public StringHttpClient setRequestLogger(RequestLogger requestLogger) {
        this.requestLogger = requestLogger;
        return this;
    }
    
    public StringHttpClient setResponseLogger(ResponseLogger responseLogger) {
        this.responseLogger = responseLogger;
        return this;
    }

    public StringHttpClient setRequestEncoder(RequestEncoder requestEncoder) {
        this.requestEncoder = requestEncoder;
        return this;
    }
    
    public StringHttpClient setResponseReader(ResponseReader responseReader) {
        this.responseReader = responseReader;
        return this;
    }

    public String doPost(String request) throws Exception {
        return doPost(request, false);
    }

    public String doPost(String request, boolean compress) throws Exception {
        byte[] body;
        if (requestEncoder != null) {
            body = requestEncoder.encode(this, request, compress);
        } else {
            body = DEFAULT_REQUEST_ENCODER.encode(this, request, compress);
        }
        HttpURLConnection conn = openConnection(METHOD_POST, new URL(url), compress);
        try {
            if (requestLogger != null) {
                requestLogger.log(requestHeader(), url, request);
            }
            if (body != null) {
                conn.getOutputStream().write(body);
            } else {
                conn.setRequestProperty(PROPERTY_CONTENT_LEN, "0");
            }
            conn.getOutputStream().flush();
            conn.getOutputStream().close();
            if (sentLogger != null) {
                sentLogger.log(requestHeader(), url, request);
            }
            return handleResponse(conn);
        } finally {
            conn.disconnect();
        }
    }

    public String doGet() throws Exception {
        HttpURLConnection conn = openConnection(METHOD_GET, new URL(url), false);
        try {
            if (requestLogger != null) {
                requestLogger.log(requestHeader(), url, null);
            }
            conn.connect();
            if (sentLogger != null) {
                sentLogger.log(requestHeader(), url, null);
            }
            return handleResponse(conn);
        } finally {
            conn.disconnect();
        }
    }
    
    private String handleResponse(HttpURLConnection conn) throws Exception {
        int code = conn.getResponseCode();
        if (code != 200) {
            String message = conn.getResponseMessage();
            StringBuilder error = new StringBuilder();
            if (code >= 400) {
                InputStream is = conn.getErrorStream();
                if (is != null) {
                    String lineSeparator = System.getProperty("line.separator");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, getCharset()));
                    boolean first = true;
                    String line;
                    while ((line = reader.readLine()) != null) {
                        if (first) {
                            first = false;
                        } else {
                            error.append(lineSeparator);
                        }
                        error.append(line);
                    }
                }
            }
            if (responseLogger != null) {
                responseLogger.log(responseHeader(conn), url, code, message);
            }
            if (error.length() > 0) {
                throw new ResponseException(code, message, error.toString());
            } else {
                throw new ResponseException(code, message);
            }
        }
        InputStream ips;
        if (GZIP.equalsIgnoreCase(conn.getHeaderField(PROPERTY_CONTENT_ENCODING))) {
            ips = new GZIPInputStream(conn.getInputStream());
        } else {
            ips = conn.getInputStream();
        }
        if (responseReader == null) {
            responseReader = new SimpleResponseReader();
        }
        String responseJson = responseReader.read(ips);
        if (responseLogger != null) {
            responseLogger.log(responseHeader(conn), url, code, responseJson);
        }
        return responseJson;
    }
    
    protected HttpURLConnection openConnection(String method, URL url, boolean compress) throws Exception {
        HttpURLConnection conn = Networks.connect(url, sslContext, hostNameVerifier);
        conn.setRequestMethod(method);
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setConnectTimeout(connectTimeout);
        conn.setReadTimeout(readTimeout);

        conn.setRequestProperty(PROPERTY_CONNECTION, CONNECTION_KEEP_ALIVE);
        if (compress) {
            conn.setRequestProperty(PROPERTY_CONTENT_ENCODING, GZIP);
            conn.setRequestProperty(PROPERTY_CONTENT_TYPE, CONTENT_TYPE_APPLICATION_OCTETSTREAM);
        } else {
            conn.setRequestProperty(PROPERTY_CONTENT_TYPE, contentType + "; charset=" + getCharset());
        }
        if (acceptCompress) {
            conn.setRequestProperty(PROPERTY_ACCEPT_ENCODING, GZIP);
        }
        
        for (Map.Entry<String, String> entry : httpProperties.entrySet()) {
            conn.setRequestProperty(entry.getKey(), entry.getValue());
        }

        return conn;
    }

    private Header requestHeader() {
        return new Header() {
            @Override
            public String get(String key) {
                return httpProperties.get(key);
            }
        };
    }

    private Header responseHeader(final HttpURLConnection conn) {
        return new Header() {
            @Override
            public String get(String key) {
                return conn.getHeaderField(key);
            }
        };
    }
    
    public static StringHttpClient forRequest(String url) {
        return new StringHttpClient(url);
    }

    /**
     *
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     */
    public interface RequestEncoder {
        byte[] encode(StringHttpClient client, String request, boolean compress) throws Exception;
    }

    /**
     *
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     */
    public static class SimpleRequestEncoder implements RequestEncoder {

        @Override
        public byte[] encode(StringHttpClient client, String request, boolean compress) throws Exception {
            if (request == null) {
                return null;
            }

            String charset = client.getCharset();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            if (compress) {
                GZIPOutputStream gzipOut = new GZIPOutputStream(out);
                gzipOut.write(request.getBytes(charset));
                gzipOut.finish();
                gzipOut.close();
            } else {
                out.write(request.getBytes(charset));
            }
            return out.toByteArray();
        }
    }

    /**
     *
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     */
    public static class SimpleResponseReader implements ResponseReader {

        @Override
        public String read(InputStream is) throws Exception {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            Streams.transfer(is, os);
            return new String(os.toByteArray(), StandardCharsets.UTF_8);
        }
    }

    /**
     *
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     */
    public interface ResponseReader {
        String read(InputStream ips) throws Exception;
    }

    public interface Header {
        String get(String key);
    }

    /**
     *
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     */
    public interface RequestLogger {
        void log(Header header, String url, String text);
    }

    public interface ResponseLogger {
        void log(Header header, String url, int status, String text);
    }

}
