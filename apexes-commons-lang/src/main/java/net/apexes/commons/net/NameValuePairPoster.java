/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.net;

import net.apexes.commons.lang.Checks;
import net.apexes.commons.lang.Networks;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.util.Map;

/**
 *
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class NameValuePairPoster {

    public static final String CONTENT_TYPE = "application/x-www-form-urlencoded";

    private final StringHttpClient client;

    public NameValuePairPoster(String url) {
        this.client = new StringHttpClient(url, CONTENT_TYPE);
    }

    public NameValuePairPoster setConnectTimeout(int timeout) {
        client.setConnectTimeout(timeout);
        return this;
    }

    public NameValuePairPoster setReadTimeout(int timeout) {
        client.setReadTimeout(timeout);
        return this;
    }

    public NameValuePairPoster setSslContext(SSLContext sslContext) {
        client.setSslContext(sslContext);
        return this;
    }

    public NameValuePairPoster setHostNameVerifier(HostnameVerifier hostNameVerifier) {
        client.setHostNameVerifier(hostNameVerifier);
        return this;
    }

    public NameValuePairPoster setHttpProperty(String key, String value) {
        client.setHttpProperty(key, value);
        return this;
    }

    public NameValuePairPoster setRequestLogger(StringHttpClient.RequestLogger requestLogger) {
        client.setRequestLogger(requestLogger);
        return this;
    }

    public NameValuePairPoster setResponseLogger(StringHttpClient.ResponseLogger responseLogger) {
        client.setResponseLogger(responseLogger);
        return this;
    }

    public NameValuePairPoster setRequestEncoder(StringHttpClient.RequestEncoder requestEncoder) {
        client.setRequestEncoder(requestEncoder);
        return this;
    }

    public NameValuePairPoster setResponseReader(StringHttpClient.ResponseReader responseReader) {
        client.setResponseReader(responseReader);
        return this;
    }

    public String post(Map<String, String> request) throws Exception {
        return post(request, null);
    }

    public String post(Map<String, String> request, String charset) throws Exception {
        Checks.verifyNotNull(request, "request");

        if (charset != null) {
            client.setCharset(charset);
        }

        String body = Networks.forNameValuePair(request.entrySet(), client.getCharset());
        return client.doPost(body);
    }

    public static NameValuePairPoster forRequest(String url) {
        return new NameValuePairPoster(url);
    }

}
