/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import java.util.Collection;
import java.util.Map;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public final class Checks {

    private Checks() {
    }

    public static <T> T verifyNotNull(T reference) {
        if (reference == null) {
            throw new IllegalArgumentException();
        }
        return reference;
    }

    public static void verifyNotNull(Object reference, String argName) {
        if (reference == null) {
            throw new IllegalArgumentException("The " + argName + " is must be no null.");
        }
    }

    public static void verifyNotEmpty(Collection<?> collection, String argName) {
        verifyNotNull(collection, argName);
        if (collection.isEmpty()) {
            throw new IllegalArgumentException("The " + argName + " is must be no empty.");
        }
    }

    public static <T> void verifyNotEmpty(T[] array, String argName) {
        verifyNotNull(array, argName);
        if (array.length == 0) {
            throw new IllegalArgumentException("The " + argName + " is must be no empty.");
        }
    }

    public static void verifyNotEmpty(String value, String argName) {
        verifyNotNull(value, argName);
        if (value.isEmpty()) {
            throw new IllegalArgumentException("The " + argName + " is must be no empty.");
        }
    }

    public static boolean isBlank(String str) {
        return str == null || str.trim().isEmpty();
    }

    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }

    public static <T extends Collection<?>> boolean isEmpty(T collection) {
        return collection == null || collection.isEmpty();
    }

    public static <T> boolean isEmpty(T[] array) {
        return array == null || array.length == 0;
    }

    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static <T extends Collection<?>> boolean isNotEmpty(T collection) {
        return !isEmpty(collection);
    }

    public static <T> boolean isNotEmpty(T[] array) {
        return !isEmpty(array);
    }

    public static boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }

}
