/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * 62进制
 *
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class Radix62 {
    
    public static final char[] ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
    
    private static final int BASE = 62;
    private static final int MAX_ALPHABET = 'z';
    private static final int[] DIGITS_INDEX = new int[MAX_ALPHABET + 1];
    
    static {
        for (int i = 0; i < MAX_ALPHABET; i++) {
            DIGITS_INDEX[i] = -1;
        }
        for (int i = 0; i < BASE; i++) {
            DIGITS_INDEX[ALPHABET[i]] = i;
        }
    }

    /**
     * 将num转为62进制字符串显示。此方法与<code>{@link #decode(String)}</code>互逆
     * @param num 数值
     * @return 返回62进制字符串显示
     */
    public static String encode(long num) {
        if (num == 0) {
            return "0";
        }
        boolean negative = false;
        if (num < 0) {
            negative = true;
            num = -num;
        }
        StringBuilder buf = new StringBuilder();
        while (num != 0) {
            buf.append(ALPHABET[(int) (num % BASE)]);
            num /= BASE;
        }
        if (negative) {
            buf.append("-");
        }
        return buf.reverse().toString();
    }

    /**
     * 将62进制字符串转为数值。此方法与<code>{@link #encode(long)}</code>互逆
     * @param str 62进制字符串
     * @return 返回数值
     */
    public static long decode(String str) {
        boolean negative = false;
        if (str.charAt(0) == '-') {
            str = str.substring(1, str.length());
            negative = true;
        }
        long num = 0L;
        long multiplier = 1;
        for (int pos = str.length() - 1; pos >= 0; pos--) {
            num += getIndex(str, pos) * multiplier;
            multiplier *= BASE;
        }
        return negative ? -num : num;
    }
    
    private static int getIndex(String s, int pos) {
        char c = s.charAt(pos);
        if (c > MAX_ALPHABET) {
            throw new IllegalArgumentException("Unknow character for Radix62: " + s);
        }
        int index = DIGITS_INDEX[c];
        if (index == -1) {
            throw new IllegalArgumentException("Unknow character for Radix62: " + s);
        }
        return index;
    }
    
}
