package net.apexes.commons.lang;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public final class Collects {

    private Collects() {}

    /**
     * 将指定集合分折成多个大小不大于 limit 的小集合
     * @param srcs
     * @param batchLimit 每个小集合中的最大元素数量
     * @return
     */
    public static <T> List<Set<T>> splitLimit(Collection<T> srcs, int batchLimit) {
        List<Set<T>> resultList = new ArrayList<>();
        Set<T> subSet = new HashSet<>();
        for (T src : srcs) {
            subSet.add(src);
            if (subSet.size() >= batchLimit) {
                resultList.add(subSet);
                subSet = new HashSet<>();
            }
        }
        if (Checks.isNotEmpty(subSet)) {
            resultList.add(subSet);
        }
        return resultList;
    }

    /**
     * 按指定数量拆分小集合
     * @param srcs
     * @param splitCount 拆分出的小集合的最大个数
     * @return
     */
    public static <T> List<Set<T>> splitCount(Collection<T> srcs, int splitCount) {
        List<Set<T>> list = new ArrayList<>();
        int size = Math.min(srcs.size(), splitCount);
        for (int i = 0; i < size; i++) {
            list.add(new HashSet<T>());
        }
        int i = 0;
        for (T src : srcs) {
            if (i == size) {
                i = 0;
            }
            Set<T> subSet = list.get(i);
            subSet.add(src);
            i++;
        }
        return list;
    }

    /**
     * 按指定数量拆分 Map
     * @param map
     * @param splitCount 拆分出的小 Map 的最大个数
     * @return
     */
    public static <K, V> List<Map<K, V>> splitCount(Map<K, V> map, int splitCount) {
        List<Map<K, V>> list = new ArrayList<>();
        int size = Math.min(map.size(), splitCount);
        for (int i = 0; i < size; i++) {
            list.add(new LinkedHashMap<K, V>());
        }
        int i = 0;
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (i == size) {
                i = 0;
            }
            Map<K, V> subMap = list.get(i);
            subMap.put(entry.getKey(), entry.getValue());
            i++;
        }
        return list;
    }
}
