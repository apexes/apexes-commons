package net.apexes.commons.lang;

import java.util.Map;

/**
 * @author hedyn
 */
public class NetworkTimeMillisException  extends Exception {

    public NetworkTimeMillisException(Exception cause) {
        super(cause);
    }

    public NetworkTimeMillisException(Map<String, Long> timeMap, Map<String, Exception> causeMap) {
        super(message(timeMap, causeMap));
        for (Exception cause : causeMap.values()) {
            addSuppressed(cause);
        }
    }

    public boolean isSuppressedWith(Class<? extends Throwable> throwableClass) {
        for (Throwable cause : getSuppressed()) {
            if (throwableClass.isAssignableFrom(cause.getClass())) {
                return true;
            }
        }
        return false;
    }

    private static String message(Map<String, Long> timeMap, Map<String, Exception> causeMap) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, Long> entry : timeMap.entrySet()) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }
            sb.append(entry.getKey());
            sb.append(" = ");
            sb.append(entry.getValue());
        }
        for (Map.Entry<String, Exception> entry : causeMap.entrySet()) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }
            sb.append(entry.getKey());
            sb.append(" = ");
            sb.append(entry.getValue().getClass().getSimpleName());
        }
        return sb.toString();
    }
}
