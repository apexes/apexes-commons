package net.apexes.commons.lang;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class Platforms {

    public static boolean isLinux() {
        return "linux".equals(getOS());
    }

    public static boolean isWindows() {
        return "win32".equals(getOS());
    }

    public static boolean isMacos() {
        return "darwin".equals(getOS());
    }

    /**
     * Returns the platform name. This could be for example "linux-x86" or
     * "windows-x86_64".
     *
     * @return The architecture name. Never null.
     */
    public static String getPlatform() {
        return getOS() + "-" + getArch();
    }

    /**
     * Returns the operating system name. This could be "linux", "windows" or
     * "macos" or (for any other non-supported platform) the value of the
     * "os.name" property converted to lower case and with removed space
     * characters.
     *
     * @return The operating system name.
     */
    public static String getOS() {
        String os = System.getProperty("os.name").toLowerCase().replace(" ", "");
        if (os.contains("windows")) {
            return "win32";
        }
        if ("macosx".equals(os) || "macos".equals(os)) {
            return "darwin";
        }
        return os;
    }

    /**
     * Returns the CPU architecture. This will be "x86" or "x86_64" (Platform
     * names i386 und amd64 are converted accordingly) or (when platform is
     * unsupported) the value of os.arch converted to lower-case and with
     * removed space characters.
     *
     * @return The CPU architecture
     */
    public static String getArch() {
        String arch = System.getProperty("os.arch").toLowerCase().replace(" ", "");
        if ("i386".equals(arch)) {
            return "x86";
        }
        if ("amd64".equals(arch) || "x86_64".equals(arch)) {
            return "x86-64";
        }
        if ("arm64".equals(arch)) {
            return "aarch64";
        }
        if ("armhf".equals(arch) || "aarch32".equals(arch) || "armv7l".equals(arch)) {
            return "arm";
        }
        return arch;
    }

}
