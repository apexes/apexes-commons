/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import net.apexes.commons.lang.ext.JseProcessIdentifier;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface ProcessIdentifier {

    short processId();

    /**
     * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
     */
    class Impl {

        private static volatile Short processId;

        public static void set(ProcessIdentifier processIdentifier) {
            synchronized (Impl.class) {
                processId = processIdentifier.processId();
            }
        }

        public static short processId() {
            if (processId == null) {
                synchronized (Impl.class) {
                    if (processId == null) {
                        processId = JseProcessIdentifier.myPid();
                    }
                }
            }
            return processId;
        }
    }
}
