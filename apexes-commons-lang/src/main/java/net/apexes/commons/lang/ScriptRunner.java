package net.apexes.commons.lang;

import java.sql.Connection;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 * @deprecated 使用 {@link SQLScriptRunner}
 */
@Deprecated
public class ScriptRunner extends SQLScriptRunner {

    public ScriptRunner(Connection connection, boolean autoCommit, boolean stopOnError) {
        super(connection, autoCommit, stopOnError);
    }
}
