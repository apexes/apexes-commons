/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import java.math.BigDecimal;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class Decimal extends BigDecimal {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public static final Decimal HUNDRED = Decimal.valueOf(BigDecimal.valueOf(100));

    private Decimal(BigDecimal value) {
    	super(Numbers.trimZeroString(value));
    }
    
    @Override
    public String toString() {
        return toPlainString();
    }

    /**
     * 
     * @param value
     * @return
     */
    public static Decimal valueOf(BigDecimal value) {
        if (value == null) {
            return null;
        }
        return new Decimal(value);
    }
    
}
