package net.apexes.commons.lang;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author hedyn
 */
class NetworkTimeMillis {
    private NetworkTimeMillis() {}

    private static final Logger LOG = Logger.getLogger(NetworkTimeMillis.class.getName());

    static long invoke(ExecutorService executor, int timeoutMs, String... urls) throws NetworkTimeMillisException {
        Checks.verifyNotNull(executor, "executor");
        Checks.verifyNotEmpty(urls, "urls");

        List<Result> resultList = new ArrayList<>();
        try {
            List<NetworkTimeMillisTask> taskList = new ArrayList<>();
            for (String url : urls) {
                taskList.add(new NetworkTimeMillisTask(url, timeoutMs));
            }
            long timeout;
            if (timeoutMs > 0) {
                timeout = timeoutMs * 2L + 500;
            } else {
                timeout = timeoutMs;
            }
            List<Future<Result>> futureList = executor.invokeAll(taskList);
            for (Future<Result> future : futureList) {
                resultList.add(future.get(timeout, TimeUnit.MILLISECONDS));
            }
        } catch (Exception e) {
            throw new NetworkTimeMillisException(e);
        }

        Map<String, Long> timeMap = new LinkedHashMap<>();
        Map<String, Exception> causeMap = new LinkedHashMap<>();
        for (Result result : resultList) {
            if (result.exception != null) {
                causeMap.put(result.url, result.exception);
            } else if (result.timeMillis != 0) {
                timeMap.put(result.url, result.timeMillis);
            }
        }

        if (urls.length == 1) {
            if (timeMap.isEmpty()) {
                throw new NetworkTimeMillisException(timeMap, causeMap);
            }
            return timeMap.get(urls[0]);
        }

        if (LOG.isLoggable(Level.INFO)) {
            LOG.log(Level.INFO, "networkTimeMillis: {0}", timeMap);
        }

        if (timeMap.size() >= 2) {
            List<Long> timeList = new ArrayList<>(timeMap.values());
            Long minDiff = null;
            Long select1 = null;
            Long select2 = null;
            for (int i = timeList.size() - 1; i > 0; i--) {
                for (int j = i - 1; j >= 0; j--) {
                    long value1 = timeList.get(i);
                    long value2 = timeList.get(j);
                    long diff = Math.abs(value1 - value2);
                    if (diff == 0) {
                        return value1;
                    }
                    if (minDiff == null || minDiff > diff) {
                        minDiff = diff;
                        select1 = value1;
                        select2 = value2;
                    }
                }
            }
            if (minDiff != null && minDiff < 30000) {
                return Math.max(select1, select2);
            }
        }

        throw new NetworkTimeMillisException(timeMap, causeMap);
    }

    /**
     *
     * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
     */
    private static class NetworkTimeMillisTask implements Callable<Result> {

        private final String url;
        private final int timeoutMs;

        private NetworkTimeMillisTask(String url, int timeoutMs) {
            this.url = url;
            this.timeoutMs = timeoutMs;
        }

        @Override
        public Result call() throws Exception {
            if (url.startsWith("https://")) {
                HttpURLConnection conn = null;
                try {
                    conn = Networks.connectExemptSSL(new URL(url));
                    conn.setConnectTimeout(timeoutMs);
                    conn.setReadTimeout(timeoutMs);
                    conn.setUseCaches(false);
                    conn.connect();
                    return new Result(url, conn.getDate());
                } catch (Exception e) {
                    return new Result(url, e);
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
            } else {
                try {
                    URLConnection conn = new URL(url).openConnection();
                    conn.setConnectTimeout(timeoutMs);
                    conn.setReadTimeout(timeoutMs);
                    conn.setUseCaches(false);
                    conn.connect();
                    return new Result(url, conn.getDate());
                } catch (Exception e) {
                    return new Result(url, e);
                }
            }
        }
    }

    private static class Result {

        private final String url;
        private final Long timeMillis;
        private final Exception exception;

        private Result(String url, long timeMillis) {
            this.url = url;
            this.timeMillis = timeMillis;
            this.exception = null;
        }

        private Result(String url, Exception exception) {
            this.url = url;
            this.timeMillis = null;
            this.exception = exception;
        }
    }
}
