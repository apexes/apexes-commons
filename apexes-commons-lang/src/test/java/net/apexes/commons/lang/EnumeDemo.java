/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class EnumeDemo {
    
    public static void main(String[] args) {
        test(Enume.fromValue(EnumeStatus.class, "un"));
        printGc("main");
    }

    private static void test(EnumeStatus un) {
        System.out.println(un.value() + ", " + un.name() + ", " + un.isUnknown() + ", " + un.ordinal());

        System.out.println("EnumeTestTypeBase: " + Enume.valueClass(EnumeTestTypeBase.class));
        System.out.println("EnumeTestType: " + Enume.valueClass(EnumeTestType.class));
        System.out.println("EnumeBool: " + Enume.valueClass(EnumeBool.class));
        System.out.println("EnumeBool2: " + Enume.valueClass(EnumeBool2.class));
        System.out.println("EnumeStatus: " + Enume.valueClass(EnumeStatus.class));
        System.out.println("String: " + Enume.valueClass(String.class));

        EnumeStatus valid = Enume.fromValue(EnumeStatus.class, "V");
        System.out.println(valid.value() + ", " + valid.name() + ", " + valid.fullName() + ", " + valid.isUnknown() + ", " + valid.ordinal());

        EnumeStatus all = Enume.fromValue(EnumeStatus.class, "__ALL__");
        System.out.println(all.value() + ", " + all.name() + ", " + all.fullName() + ", " + all.isUnknown() + ", " + all.ordinal());

        System.out.println(Enume.values(EnumeStatus.class));
        for (EnumeStatus enumeStatus : Enume.values(EnumeStatus.class)) {
            System.out.println(enumeStatus + ", name=" + enumeStatus.name() + ", fullName=" + enumeStatus.fullName()
                    + ", ordinal=" + enumeStatus.ordinal());
        }

        test1();
        printGc("test");
    }
    
    private static void test1() {
        EnumeStatus typeValid = EnumeStatus.VALID;
        System.out.println("typeValid: " + typeValid + ", name=" + typeValid.name() + ", fullName=" + typeValid.fullName()
                + ", ordinal=" + typeValid.ordinal());
        assert(typeValid.eqValue("V"));
        
        EnumeStatus unknownType = Enume.fromValue(EnumeStatus.class, "A");
        System.out.println("unknownType: " + unknownType + ", name=" + unknownType.name() + ", fullName=" + unknownType.fullName()
                + ", ordinal=" + unknownType.ordinal());
        assert(!typeValid.eqValue(Enume.toValue(unknownType)));
        
        EnumeStatus type2 = EnumeStatus.VALID;
        assert(typeValid == type2);
        
        EnumeStatus type3 = type2;
        assert(type2 == type3);
        
        EnumeStatus unknownType2 = Enume.fromValue(EnumeStatus.class, "V");
        System.out.println("unknownType2: " + unknownType2 + ", name=" + unknownType2.name() + ", fullName=" + unknownType2.fullName()
                + ", ordinal=" + unknownType2.ordinal());
        assert(typeValid == unknownType2);
        assert(typeValid.equals(unknownType2));
        assert(!typeValid.equals(new Object()));
        
        Enume.valueOf(EnumeStatus.class, "B");
        Enume.valueOf(EnumeStatus.class, "C");
        
        EnumeBool yes = EnumeBool.YES;
        EnumeBool no = EnumeBool.NO;
        assert(!yes.equals(no));
        
        EnumeBool unknownBool = Enume.fromValue(EnumeBool.class, -1);
        assert(!yes.equals(unknownBool));

        printGc("test1");
    }

    private static void printGc(String name) {
        System.out.println(name + " gc =====1");
        Enume.printCache(System.out);
        System.gc();
        Enume.printCache(System.out);
        System.out.println(name + " gc =====2");
    }
    
}
