/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import java.util.Arrays;

/**
 * @author hedyn
 */
public class AlgorithmsDemo {

    public static void main(String[] args) {
        testCombination();
        testPermutation();
    }

    private static long combinationCount = 0;
    private static void testCombination() {
        System.out.println("----------- testCombination -----------");
        int options = 5;
        int select = 3;
        long time = System.currentTimeMillis();
        Algorithms.combination(options, select, new Algorithms.IntArrayConsumer() {

            @Override
            public boolean accept(int[] indexs) {
                System.out.printf("%02d%s\n", (combinationCount + 1), Arrays.toString(indexs));
                combinationCount++;
                return false;
            }
        });
        System.out.printf("C[%d,%d]=%d [%dms]\n", options, select, combinationCount, (System.currentTimeMillis() - time));
        // C[5,3]=10 [5ms]
        // C[6,3]=20 [3ms]
        // C[30,10]=30045015 [282ms]
        // C[40,10]=847660528 [6047ms]
    }

    private static long permutationCount = 0;
    private static void testPermutation() {
        System.out.println("----------- testPermutation -----------");
        int options = 5;
        long time = System.currentTimeMillis();
        Algorithms.permutation(options, new Algorithms.IntArrayConsumer() {

            @Override
            public boolean accept(int[] indexs) {
                System.out.printf("%02d%s\n", (permutationCount + 1), Arrays.toString(indexs));
                permutationCount++;
                return false;
            }
        });
        System.out.printf("P[%d]=%d [%dms]\n", options, permutationCount, (System.currentTimeMillis() - time));
        // P[5]=120 [1ms]
        // P[3]=6 [2ms]
        // P[6]=720 [1ms]
        // P[10]=3628800 [54ms]
        // P[11]=39916800 [369ms]
        // P[12]=479001600 [4191ms]
    }
}
