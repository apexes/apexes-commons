/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public final class EnumeBool extends Enume.This<Integer, EnumeBool> {
    
    public static final EnumeBool YES = new EnumeBool(1);
    
    public static final EnumeBool NO = new EnumeBool(0);
    
    private EnumeBool(Integer value) {
        super(value);
    }

}
