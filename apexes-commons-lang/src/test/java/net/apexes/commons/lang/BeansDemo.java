/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class BeansDemo {

    public static void main(String[] args) throws Exception {
        Bean1 bean1 = new Bean1();
        bean1.setId("a123");
        bean1.setStatus(EnumeStatus.VALID);
        bean1.setBool(EnumeBool.YES);
        bean1.setTime(System.currentTimeMillis());
        bean1.setStatus2(EnumeStatus.VALID);

        Bean2 bean2 = new Bean2();
        Bean1 bean3 = new Bean1();

        Beans.BeanCopier copier = Beans.copier()
                .fromEnume()
                .toEnume(EnumeStatus.class)
                .toEnume(EnumeBool.class);

        copier.copy(bean1, bean2, "time");

        System.out.println(bean1);
        System.out.println(bean2);

        copier.copy(bean2, bean3, "time");

        System.out.println(bean3);
    }

    /**
     *
     */
    public static class Bean1 {

        private String id;
        private EnumeStatus status;
        private EnumeBool bool;
        private int number;
        private Long time;
        private EnumeStatus status2;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public EnumeStatus getStatus() {
            return status;
        }

        public void setStatus(EnumeStatus status) {
            this.status = status;
        }

        public EnumeBool getBool() {
            return bool;
        }

        public void setBool(EnumeBool bool) {
            this.bool = bool;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public Long getTime() {
            return time;
        }

        public void setTime(Long time) {
            this.time = time;
        }

        public EnumeStatus getStatus2() {
            return status2;
        }

        public void setStatus2(EnumeStatus status2) {
            this.status2 = status2;
        }

        @Override
        public String toString() {
            return "Bean1{" +
                    "id='" + id + '\'' +
                    ", status=" + status +
                    ", bool=" + bool +
                    ", number=" + number +
                    ", time=" + time +
                    ", status2=" + status2 +
                    '}';
        }
    }

    /**
     *
     */
    public static class Bean2 {

        private String id;
        private String status;
        private Integer bool;
        private int number;
        private Long time;
        private EnumeStatus status2;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Integer getBool() {
            return bool;
        }

        public void setBool(Integer bool) {
            this.bool = bool;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public Long getTime() {
            return time;
        }

        public void setTime(Long time) {
            this.time = time;
        }

        public EnumeStatus getStatus2() {
            return status2;
        }

        public void setStatus2(EnumeStatus status2) {
            this.status2 = status2;
        }

        @Override
        public String toString() {
            return "Bean2{" +
                    "id='" + id + '\'' +
                    ", status='" + status + '\'' +
                    ", bool=" + bool +
                    ", number=" + number +
                    ", time=" + time +
                    ", status2=" + status2 +
                    '}';
        }
    }
}
