/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class ReflectDemo {
    
    public static void main(String[] args) {
//        testInvokeClassName();

        System.out.println(Reflect.on(R.anim.class).field("abc_fade_in").get());
        System.out.println(Reflect.on(R.class.getName() + "$string").field("PORT_NUMBER").get());
    }

    private static void testInvokeClassName() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("lambda: " + Reflect.getInvokeClassName());
            }
        }).start();
        new Thread() {
            public void run() {
                System.out.println("thread: " + Reflect.getInvokeClassName());
            }
        }.start();
        System.out.println("main: " + Reflect.getInvokeClassName());
    }

    /**
     * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
     */
    public static class R {

        public static final class anim {
            public static final int abc_fade_in=10;
            public static final int abc_fade_out=20;
        }

        public static final class string {
            public static final int PORT_NUMBER = 1;
            public static final int abc_action_bar_home_description = 2;
            public static final int abc_action_bar_up_description = 3;
        }
    }

}
