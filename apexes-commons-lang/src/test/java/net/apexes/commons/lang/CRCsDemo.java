/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class CRCsDemo {

    public static void main(String[] args) {
        for (int i = 0; i < 50; i++) {
            String str = IDs.randomBase58UUID();
            int crc16 = CRCs.crc16(str.getBytes());
            byte crc8 = CRCs.crc8(str.getBytes());
            System.out.println(String.format("%s >> CRC8=%03d, CRC16=%d", str, (crc8&0xff), crc16));
        }
    }
}
