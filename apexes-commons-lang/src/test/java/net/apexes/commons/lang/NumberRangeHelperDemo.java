/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class NumberRangeHelperDemo {

    public static void main(String[] args) {
        String str;
        int qty;
        NumberRangeHelper helper;
        str = "123456789012345678900";
        qty = 100;
        helper = new NumberRangeHelper(str, qty);
        System.out.println(str + " + " + qty + ": " + helper.isValid() + ": " + helper);
        str = "012AC0000";
        qty = 100;
        helper = new NumberRangeHelper(str, qty);
        System.out.println(str + " + " + qty + ": " + helper.isValid() + ": " + helper);
        str = "012SCEA01";
        qty = 98;
        helper = new NumberRangeHelper(str, qty);
        System.out.println(str + " + " + qty + ": " + helper.isValid() + ": " + helper);
        str = "012SCEA01";
        qty = 100;
        helper = new NumberRangeHelper(str, qty);
        System.out.println(str + " + " + qty + ": " + helper.isValid() + ": " + helper);
        str = "F012EAB5";
        qty = 6;
        helper = new NumberRangeHelper(str, qty);
        System.out.println(str + " + " + qty + ": " + helper.isValid() + ": " + helper);
        str = "012EF025";
        qty = 5;
        helper = new NumberRangeHelper(str, qty);
        System.out.println(str + " + " + qty + ": " + helper.isValid() + ": " + helper);
        System.out.println(java.util.Arrays.toString(helper.generateNumbers()));

        str = "F012EAB5";
        String endRef = "012SCEA9";
        helper = new NumberRangeHelper(str, endRef);
        System.out.println(str + " ~ " + endRef + ": " + helper.isValid() + ": " + helper);
        str = "5274627321005470661 ";
        endRef = "5274627321005470661 ";
        helper = new NumberRangeHelper(str, endRef);
        System.out.println(str + " ~ " + endRef + ": " + helper.isValid() + ": " + helper);
        System.out.println(java.util.Arrays.toString(helper.generateNumbers()));
        str = "012SCEA01";
        endRef = "012SCEA09";
        helper = new NumberRangeHelper(str, endRef);
        System.out.println(str + " ~ " + endRef + ": " + helper.isValid() + ": " + helper);
        System.out.println(java.util.Arrays.toString(helper.generateNumbers()));

    }

}
