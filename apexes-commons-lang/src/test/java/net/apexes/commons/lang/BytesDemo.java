/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class BytesDemo {

    public static void main(String[] args) {
        byte[] longBytes = Bytes.longToBytes(2L);
        for (int i = 0; i < longBytes.length; i++) {
            System.out.println(i + " : " + longBytes[i]);
        }
        long longValue = Bytes.bytesToLong(longBytes);
        System.out.println("long : " + longValue + " = " + Bytes.toHex(longBytes));

        byte[] intBytes = Bytes.intToBytes(2);
        long intValue = Bytes.bytesToInt(intBytes);
        System.out.println("int : " + intValue + " = " + Bytes.toHex(intBytes));

        byte[] shortBytes = Bytes.shortToBytes((short) 2);
        long shortValue = Bytes.bytesToShort(shortBytes);
        System.out.println("short : " + shortValue + " = " + Bytes.toHex(shortBytes));
    }
}
