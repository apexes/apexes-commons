/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class ConsistenthashHelperDemo {

    public static void main(String[] args) {
        List<String> sources = new ArrayList<>();
        for (int i = 500; i < 1000; i++) {
            for (int j = 0; j < 300; j++) {
                sources.add(String.format("0%03d", i) + "|1" + String.format("%03d", j));
            }
        }

        Map<String, Integer> counter = new TreeMap<>();
        ConsistenthashHelper helper = new ConsistenthashHelper();
//        helper.add("192.168.0.0");
//        helper.add("192.168.0.1");
//        helper.add("192.168.0.2");
//        helper.add("192.168.0.3");
//        helper.add("192.168.0.4");
        helper.add(IDs.newId());
        helper.add(IDs.newId());
        helper.add(IDs.newId());
        helper.add(IDs.newId());
        helper.add(IDs.newId());

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.println(helper.get("source" + i));
            }
            System.out.println("# " + helper.get("source" + i));
        }

        System.out.println("---------------------");

        for (String source : sources) {
            String node = helper.get(source);
            Integer count = counter.get(node);
            if (count == null) {
                count = 0;
            }
            count++;
            counter.put(node, count);
        }
        System.out.println(counter);
    }
}
