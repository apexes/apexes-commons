/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import java.util.Arrays;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class Base58Demo {

    public static void main(String[] args) {
        long timeMillis = System.currentTimeMillis();
        System.out.println("Hex: " + Bytes.toHex(timeMillis));
        System.out.println("Hex: " + Bytes.toHex(timeMillis<<6));

        test(Secrets.md5("" + System.currentTimeMillis()));

        test(Bytes.longToBytes(timeMillis));
        test(Bytes.longToBytes(timeMillis<<6));
        test(Bytes.longToBytes(Long.MAX_VALUE));

        byte[] bs = new byte[6];
        Arrays.fill(bs, (byte)0xff);
        test(bs);

        byte[] bytes = new byte[2];
        bytes[0] = 58;
        bytes[1] = 58;
        test(bytes);
    }

    private static void test(byte[] bytes) {
        String encode = Base58.encode(bytes);
        System.out.println(Arrays.equals(bytes, Base58.decode(encode)) + " | " + encode);
    }
}
