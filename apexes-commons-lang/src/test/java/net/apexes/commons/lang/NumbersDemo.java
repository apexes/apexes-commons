/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class NumbersDemo {

    public static void main(String[] args) {
        testBit();
        testFloorOfFive();
        testFloorOfFiveByBitwise();
    }
    
    private static void testBit() {
        System.out.println("==================== testBit");
        BigDecimal value = BigDecimal.valueOf(97531.02468);
        System.out.println(value + " ------");
        for (int i = -6; i <= 6; i++) {
            if (i == 0) {
                System.out.println("------");
            } else {
                System.out.printf("%d : %d\n", i, Numbers.bit(value, i));
            }
        }
    }
    
    private static void testFloorOfFive() {
        System.out.println("==================== testFloorOfFive");
        
        BigDecimal value;
        
        value = BigDecimal.valueOf(126.6083527);
        System.out.println("---- " + Numbers.trimZeroString(value));
        for (int i = -4; i < 4; i++) {
            System.out.println(i + " : " + Numbers.trimZeroString(Numbers.floorOfFive(value, i)));
        }
        
        value = BigDecimal.valueOf(126.5);
        System.out.println("---- " + Numbers.trimZeroString(value));
        for (int i = -2; i < 2; i++) {
            System.out.println(i + " : " + Numbers.trimZeroString(Numbers.floorOfFive(value, i)));
        }
        
        value = BigDecimal.valueOf(126);
        System.out.println("---- " + Numbers.trimZeroString(value));
        for (int i = -2; i < 3; i++) {
            System.out.println(i + " : " + Numbers.trimZeroString(Numbers.floorOfFive(value, i)));
        }
    }
    
    private static void testFloorOfFiveByBitwise() {
        System.out.println("==================== floorOfFiveByBitwise");
    
        BigDecimal value = BigDecimal.valueOf(600.7);
        System.out.println(" 1 # " + Numbers.trimZeroString(value));
        List<BigDecimal> list = Numbers.floorOfFiveByBitwise(value, 1);
        for (BigDecimal v : list) {
            System.out.println("   = " + Numbers.trimZeroString(v));
        }
        
        value = BigDecimal.valueOf(626.6083527);
        System.out.println(" 4 # " + Numbers.trimZeroString(value));
        list = Numbers.floorOfFiveByBitwise(value, 4);
        for (BigDecimal v : list) {
            System.out.println("   = " + Numbers.trimZeroString(v));
        }
    
        System.out.println("-4 # " + Numbers.trimZeroString(value));
        list = Numbers.floorOfFiveByBitwise(value, -4);
        for (BigDecimal v : list) {
            System.out.println("   = " + Numbers.trimZeroString(v));
        }
    }
}

