/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class EnumeTestType extends EnumeTestTypeBase {
    
    public static final EnumeTestType YES = new EnumeTestType(1);
    
    public static final EnumeTestType NO = new EnumeTestType(0);
    
    protected EnumeTestType(Integer value) {
        super(value);
    }
}
