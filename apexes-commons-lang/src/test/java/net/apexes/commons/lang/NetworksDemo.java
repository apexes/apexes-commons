/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.net.InetAddress;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class NetworksDemo {

    private static final Logger LOG = LoggerFactory.getLogger(NetworksDemo.class);

    public static void main(String[] args) throws Exception {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
        testNetworkTimeMillis();
//        testMacAddr();
    }

    private static void testNetworkTimeMillis() {
        try {
            LOG.info("networkTimeMillis={}", Networks.networkTimeMillis());
        } catch (Exception e) {
            LOG.error("", e);
        }
    }

    private static void testMacAddr() throws Exception {
        LOG.info("MAC Address :  {} : {} {}", Networks.macString(), Networks.macValue(), Bytes.toHex(Networks.macValue()));
        LOG.info("LocalHost Address : {}", Bytes.toHex(InetAddress.getLocalHost().getAddress()));
    }
}
