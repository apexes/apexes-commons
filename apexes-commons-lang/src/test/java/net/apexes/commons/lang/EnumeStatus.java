/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public final class EnumeStatus extends Enume.This<String, EnumeStatus> {
    
    /**
     * 有效的
     */
    public static final EnumeStatus VALID = new EnumeStatus("V");
    
    /**
     * 无效的
     */
    public static final EnumeStatus INVALID = new EnumeStatus("X");
    
    private EnumeStatus(String value) {
        super(value);
    }
    
}
