/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class EnumeTestTypeBase<T extends Comparable<T>, E extends  EnumeTestTypeBase<T, E>> extends Enume.This<T, E> {
    
    protected EnumeTestTypeBase(T value) {
        super(value);
    }
}
