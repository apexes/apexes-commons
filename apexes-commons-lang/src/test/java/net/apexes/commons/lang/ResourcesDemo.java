/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import java.io.File;
import java.io.FileInputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class ResourcesDemo {

    public static void main(String[] args) throws Exception {
        try (FileInputStream fis = new FileInputStream(new File("e:/MANIFEST.MF"))) {
            Manifest manifest = new Manifest(fis);
            Attributes attr = manifest.getMainAttributes();
            System.out.println(attr.getValue("App-Version"));
            System.out.println(attr.getValue("Build-Time"));
        }

        Class<?> classType = ResourcesDemo.class;
        System.out.println("------------- " + classType);
        System.out.println("ProjectPath : " + Resources.getProjectPath(classType));
        System.out.println("JarFile     : " + Resources.getJarFile(classType));

        classType = Resources.class;
        System.out.println("------------- " + classType);
        System.out.println("ProjectPath : " + Resources.getProjectPath(classType));
        System.out.println("JarFile     : " + Resources.getJarFile(classType));

        classType = String.class;
        System.out.println("------------- " + classType);
        System.out.println("ProjectPath : " + Resources.getProjectPath(classType));
        System.out.println("JarFile     : " + Resources.getJarFile(classType));
    }
}
