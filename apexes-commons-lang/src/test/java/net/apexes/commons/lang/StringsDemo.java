/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import java.util.Arrays;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class StringsDemo {
        
    public static void main(String[] args) {
        testStrings();
        testShorts();
        testHash();
    }


    private static void testShorts() {
        System.out.println("--------- testShorts");
        for (int i = 0; i < 10; i++) {
            String src = IDs.newId() + IDs.newId() + IDs.newId();
            String[] shorts = Strings.shorts(src);
            System.out.println(src + " | " + Arrays.toString(shorts));
        }
    }

    private static void testHash() {
        System.out.println("--------- testHash");
        for (int i = 0; i < 10; i++) {
            String src = IDs.newId() + IDs.newId() + IDs.newId();
            String hash = Strings.hash(src);
            System.out.println(src + " | " + hash);
        }
    }

    private static void testStrings() {
        System.out.println("----IDs.randomString");
        for (int i = 0; i < 10; i++) {
            System.out.println(Strings.randomString(10));
        }
        System.out.println("----IDs.randomLetter");
        for (int i = 0; i < 10; i++) {
            System.out.println(Strings.randomLetter(10));
        }
        System.out.println("----IDs.randomNumber");
        for (int i = 0; i < 10; i++) {
            System.out.println(Strings.randomNumber(6));
        }

        System.out.println("----");
        String str = "HelloWorld_";
        System.out.println(str);
        str = Strings.underscore(str);
        System.out.println(str);
        str = Strings.camelCase(str);
        System.out.println(str);
        str = Strings.underscore(str).toUpperCase();
        System.out.println(str);
        str = Strings.camelCase(str.toLowerCase());
        System.out.println(str);
    }

}
