/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class ComparatorsDemo {

    public static void main(String[] args) {
        Order order1 = new Order();
        order1.setShopId("001");
        order1.setId("zbe001");
        order1.setAmount(BigDecimal.valueOf(5.52));

        Order order2 = new Order();
        order2.setShopId("001");
        order2.setId("ead001");
        order2.setOrderNo("10011");
        order2.setAmount(BigDecimal.valueOf(4.99));

        Order order3 = new Order();
        order3.setShopId("001");
        order3.setId("aad981");
        order3.setOrderNo("10010");
        order3.setAmount(BigDecimal.valueOf(5));

        Comparators.orderBy(new Comparators.Getter<Order>() {
            @Override
            public String apply(Order o) {
                return o.getShopId();
            }
        });

        Comparators.Getter<Order> shopIdGetter = new Comparators.Getter<Order>() {
            @Override
            public String apply(Order o) {
                return o.getShopId();
            }
        };
        Comparators.Getter<Order> orderNoGetter = new Comparators.Getter<Order>() {
            @Override
            public String apply(Order o) {
                return o.getOrderNo();
            }
        };
        Comparators.Getter<Order> idGetter = new Comparators.Getter<Order>() {
            @Override
            public String apply(Order o) {
                return o.getId();
            }
        };
        Comparators.Getter<Order> amountGetter = new Comparators.Getter<Order>() {
            @Override
            public BigDecimal apply(Order o) {
                return o.getAmount();
            }
        };

        int i = Comparators
                .orderBy(shopIdGetter)
                .orderBy(orderNoGetter)
                .orderBy(idGetter)
                .orderBy(amountGetter)
                .compare(order1, order2);
        System.out.println(i);
        assert (i == -1);

        i = Comparators
                .orderBy(shopIdGetter)
                .orderBy(orderNoGetter)
                .orderBy(amountGetter)
                .orderBy(idGetter)
                .nullLast()
                .compare(order1, order2);
        System.out.println(i);
        assert (i == 1);

        i = Comparators
                .orderBy(shopIdGetter)
                .orderBy(amountGetter, false)
                .orderBy(orderNoGetter)
                .orderBy(idGetter)
                .compare(order1, order2);
        System.out.println(i);
        assert (i == -1);

        List<Order> list = Arrays.asList(order1, order2, order3);
        System.out.println(list);
        Collections.sort(list, Comparators
                .orderBy(shopIdGetter)
                .orderBy(amountGetter)
                .orderBy(orderNoGetter)
                .orderBy(idGetter));
        System.out.println(list);
    }

    /**
     *
     */
    static class Order {

        private String id;
        private String shopId;
        private String orderNo;
        private BigDecimal amount;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShopId() {
            return shopId;
        }

        public void setShopId(String shopId) {
            this.shopId = shopId;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        @Override
        public String toString() {
            return "Order{" +
                    "id='" + id + '\'' +
                    ", shopId='" + shopId + '\'' +
                    ", orderNo='" + orderNo + '\'' +
                    ", amount=" + amount +
                    '}';
        }
    }
}
