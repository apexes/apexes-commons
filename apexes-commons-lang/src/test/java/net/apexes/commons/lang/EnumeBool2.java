/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public final class EnumeBool2 extends Enume.This<Integer, EnumeBool> {

    public static final EnumeBool2 YES = new EnumeBool2(1);

    public static final EnumeBool2 NO = new EnumeBool2(0);

    private EnumeBool2(Integer value) {
        super(value);
    }

}
