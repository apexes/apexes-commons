/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import java.util.*;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class IDsDemo {
    
    public static void main(String[] args) throws Exception {
        print();
    }

    static void print() {
        int loop = 50;
        UUID uuid = UUID.randomUUID();
        String base58UUID = IDs.toBase58UUID(uuid);
        System.out.println("UUID: " + uuid + " >> " + base58UUID + "  " + (uuid.equals(IDs.formBase58UUID(base58UUID))));

        print_gen(loop);
        print_newId(loop);
        print_randomBase58UUID(loop);
        print_newObjectId(loop);
        print_objectId(loop);
    }

    static void print_gen(int loop) {
        System.out.println("----IDGenerator.gen");
        IDGenerator idGenerator = IDs.idGenerator(51410001L);
        for (int i = 0; i < loop; i++) {
            String id = idGenerator.gen();
            System.out.println(id + " : " + id.length());
        }
    }

    static void print_newId(int loop) {
        System.out.println("----IDs.newId");
        for (int i = 0; i < loop; i++) {
            String id = IDs.newId();
            System.out.println(id + " : " + id.length());
        }
    }

    static void print_randomBase58UUID(int loop) {
        System.out.println("----IDs.randomBase58UUID");
        for (int i = 0; i < loop; i++) {
            String id = IDs.randomBase58UUID();
            System.out.println(id + " : " + id.length());
        }
    }

    static void print_newObjectId(int loop) {
        System.out.println("----Ids.newObjectId");
        for (int i = 0; i < loop; i++) {
            String id = IDs.newObjectId();
            System.out.println(id + " : " + id.length());
        }
    }

    static void print_objectId(int loop) {
        System.out.println("----ObjectId");
        for (int i = 0; i < loop; i++) {
            String id = ObjectId.get().toString();
            System.out.println(id + " : " + id.length());
        }
    }

    static void check() {
        long time = System.currentTimeMillis();
        int size = 10000000;
        int batch = size / 10;
        Map<String, Integer> map = new HashMap<>(size);
        for (int i = 0; i < size; i++) {
            String id = IDs.newId();
            Integer count = map.get(id);
            if (count == null) {
                count = 0;
            } else {
                count++;
                System.out.println(id + " : " + count);
            }
            map.put(id, count);
            if (i % batch == 0) {
                System.out.println(System.currentTimeMillis()  + " : " + i);
            }
        }
        long sec = (System.currentTimeMillis() - time) / 1000;
        System.out.println(sec + "秒");
    }


}
