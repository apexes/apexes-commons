/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.lang;

import java.util.Random;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class SecretsDemo {

    public static void main(String[] args) throws Exception {
        System.out.println(Secrets.md5HexString("111asfdABCD"));
        testRadix62();
        testSkip32();
    }

    private static void testRadix62() {
        System.out.println("--------- testRadix62");
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            long num = random.nextLong();
            String str = Radix62.encode(num);
            long n = Radix62.decode(str);
            System.out.println(num + " = " + n + " | " + str);
            assert num == n;
        }
    }

    private static void testSkip32() {
        System.out.println("--------- testSkip32");
        for (int i = 0; i < 10; i++) {
            String key = Strings.randomString(10);
            int src = new Random().nextInt();
            long dest = Secrets.skip32Encrypt(src, key);
            System.out.println(key + ": " + (Secrets.skip32Decrypt(dest, key) == src) + "  " + src + " >> " + dest);
        }
    }
}
