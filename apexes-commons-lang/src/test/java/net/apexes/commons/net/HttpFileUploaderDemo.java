/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.net;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class HttpFileUploaderDemo {

    public static void main(String[] args) throws Exception {
        ByteArrayOutputStream ops = new ByteArrayOutputStream();
        FileInputStream ips = new FileInputStream("E:/a.png");
        try {
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len=ips.read(buf)) != -1) {
                ops.write(buf, 0, len);
            }
        } finally {
            ips.close();
        }
        
        HttpFileUploader uploader = new HttpFileUploader("http://v2.openapi.ele.me/image/?"
                + "consumer_key=4855494897&sig=5731cad4a7ef36ca7e2ebf545787a03b25808637&timestamp=1470377850");
        uploader.addFileParameter("file", ops.toByteArray(), "image/png");
        byte[] b = uploader.upload();
        String result = new String(b);
        System.out.println(result);
    }

}
