/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.xml;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class XmlDemo {

    public static void main(String[] args) throws Exception {
        XmlElement xml = Xmls.xml(XmlDemo.class.getResourceAsStream("demo.xml"), "utf-8");
        System.out.println(xml);
        System.out.println("--------------------------");
        XmlElement resp = Xmls.node(xml, "SOAP-ENV:Envelope/SOAP-ENV:Body/fjs1:createPoreceivingDataByDafonResponse/fjs1:response");
        XmlElement respContent = Xmls.xml(resp.getContent());
        System.out.println(respContent);
    }
}
