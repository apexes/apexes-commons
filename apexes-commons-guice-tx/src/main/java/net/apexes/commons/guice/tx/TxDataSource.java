/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.guice.tx;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class TxDataSource implements DataSource {

    public static TxDataSource of(DataSource dataSource) {
        return of("", dataSource);
    }

    public static TxDataSource of(String name, DataSource dataSource) {
        return new TxDataSource(name, dataSource);
    }

    private final String name;
    private final DataSource delegate;

    private TxDataSource(String name, DataSource delegate) {
        this.name = name;
        this.delegate = delegate;
    }

    String getName() {
        return name;
    }
    
    TxConnection createTxConnection() throws SQLException {
        return new TxConnection(delegate.getConnection());
    }
    
    TxConnection createTxConnection(String username, String password) throws SQLException {
        return new TxConnection(delegate.getConnection(username, password));
    }
    
    Connection getWithoutTxConnection() throws SQLException {
        return delegate.getConnection();
    }
    
    Connection getWithoutTxConnection(String username, String password) throws SQLException {
        return delegate.getConnection(username, password);
    }
    
    @Override
    public Connection getConnection() throws SQLException {
        return Txs.getConnection(this);
    }
    
    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return Txs.getConnection(this, username, password);
    }
    
    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return delegate.unwrap(iface);
    }
    
    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return delegate.isWrapperFor(iface);
    }
    
    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return delegate.getLogWriter();
    }
    
    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        delegate.setLogWriter(out);
    }
    
    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        delegate.setLoginTimeout(seconds);
    }
    
    @Override
    public int getLoginTimeout() throws SQLException {
        return delegate.getLoginTimeout();
    }
    
    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return delegate.getParentLogger();
    }
}
