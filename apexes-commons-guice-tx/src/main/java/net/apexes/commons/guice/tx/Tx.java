/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.guice.tx;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 启用数据库事务<br/>
 * <br/>
 * 注意事项：<br/>
 * <ul>
 * <li>只对调用方法的线程生效，在方法结束时自动提交事务（在方法中开启新线程执行的部分不会在事务中）</li>
 * <li>只能应用在 protected 或 public 作用域的类或方法上</li>
 * <li>不支持事务嵌套，被嵌套调用的方法上的注解将被忽略</li>
 * <li>在产生作用的代码区段中如果需要调用多个数据源，必须设置注解的 value 值指定要开启事务的数据源，不支持同一作用代码区段有多个数据源开启事务</li>
 * </ul>
 *
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Tx {

    /**
     * 需要开启事务的数据源
     * @return 返回需要开启事务的数据源名称，名称与{@link TxDataSource#getName()}相同
     */
    String value() default "";

    Class<? extends Throwable>[] rollback() default RuntimeException.class;

    Class<? extends Throwable>[] ignore() default {};
    
    Isolation isolation() default Isolation.DEFAULT;
    
    boolean readOnly() default false;
}
