/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.guice.tx;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public enum Isolation {
    /**
     * 读取未提交数据(会出现脏读, 不可重复读)
     */
    READ_UNCOMMITTED,
    /**
     * 读取已提交数据(会出现不可重复读和幻读)
     */
    READ_COMMITTED,
    /**
     * 可重复读(会出现幻读)
     */
    REPEATABLE_READ,
    /**
     * 串行化
     */
    SERIALIZABLE,
    /**
     * 数据库默认级别
     */
    DEFAULT;
}
