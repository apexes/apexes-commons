/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;

import com.squareup.otto.Bus;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class FilterableOttoBus {
    
    private final Bus bus;
    
    private final EventBusFilter eventBusFilter = new EventBusFilter(new IPublisher()  {
        @Override
        public <T> void publish(String address, T event) {
            bus.post(event);
        }
    });
    
    public FilterableOttoBus() {
        this(new Bus());
    }
    
    public FilterableOttoBus(Bus bus) {
        this.bus = bus;
    }
    
    public void register(Object object) {
        eventBusFilter.register(object);
        bus.register(object);
    }
    
    public void unregister(Object object) {
        bus.unregister(object);
        eventBusFilter.unregister(object);
    }
    
    public void post(Object event) {
        eventBusFilter.post(event);
    }

}
