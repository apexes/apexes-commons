/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 * @param <E>
 */
class PriorityList<E> extends CopyOnWriteArrayList<IPriority<E>> {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    public boolean add(IPriority<E> o) {
        int size = super.size();
        int index = size;
        if (index == 0) {
            return super.add(o);
        }
        for (; index > 0; index--) {
            IPriority<E> temp = super.get(index - 1);
            if (o.getPriority() - temp.getPriority() <= 0) {
                break;
            }
        }
        if (index < 0 || index >= size) {
            return super.add(o);
        } else {
            super.add(index, o);
            return true;
        }
    }
}
