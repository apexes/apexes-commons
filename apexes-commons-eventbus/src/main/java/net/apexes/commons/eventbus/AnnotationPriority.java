/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;

import java.lang.reflect.Method;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
abstract class AnnotationPriority implements IPriority<Object> {
    
    protected final Object invokedObject;
    protected final Method invokedMethod;
    
    AnnotationPriority(Object invokedObject, Method method) {
        this.invokedObject = invokedObject;
        this.invokedMethod = method;
    }
    
    protected abstract String getAddress();

}
