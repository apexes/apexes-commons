/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;

/**
 * EventBus的过滤接口
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 * @param <E>
 * @see EventFilter
 */
public interface IEventFilter<E> extends IPriority<E> {
    
    /**
     * 过滤处理event。仅当调用了chain.next(event)方法后链路的下一个过滤器才会接收到此event，链路上所有过滤器
     * 都调用了chain.next(event)方法后，处理器才能收到此event
     * @param event
     * @param chain
     */
    void filter(E event, EventFilterChain<E> chain);
    
}
