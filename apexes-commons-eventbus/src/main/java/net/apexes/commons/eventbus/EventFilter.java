/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 * @see IEventFilter
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface EventFilter {
    
    /**
     * 地址
     * @return
     */
    String address() default "";

    /**
     * 优先级，值越大优先级越高
     * @return
     */
    int priority() default 0;
}
