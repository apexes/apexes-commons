/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;


/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 * @param <E>
 */
public interface IEventHandler<E> extends IPriority<E> {
    
    /**
     * 处理event
     * @param event
     */
    void handle(E event);

}
