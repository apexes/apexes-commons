/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
interface IPriority<E> {
    
    /**
     * 返回优先级，值越大优先级越高
     * @return
     */
    int getPriority();
    
}
