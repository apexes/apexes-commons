/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;

import com.google.common.eventbus.EventBus;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class FilterableGuavaBus {
    
    private final EventBus eventBus;
    
    private final EventBusFilter eventBusFilter = new EventBusFilter(new IPublisher()  {
        @Override
        public <T> void publish(String address, T event) {
            eventBus.post(event);
        }
    });
    
    public FilterableGuavaBus() {
        this(new EventBus());
    }
    
    public FilterableGuavaBus(EventBus eventBus) {
        this.eventBus = eventBus;
    }
    
    public void register(Object object) {
        eventBusFilter.register(object);
        eventBus.register(object);
    }
    
    public void unregister(Object object) {
        eventBus.unregister(object);
        eventBusFilter.unregister(object);
    }
    
    public void post(Object event) {
        eventBusFilter.post(event);
    }

}
