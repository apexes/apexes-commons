/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 * @param <E>
 */
public interface EventFilterChain<E> {
    
    /**
     * 传给下一个EventFilter处理。如果EventFilter实现类未调用此方法，则event不会被后续的EventFilter收到
     * 
     * @param event
     */
    void next(E event);

}
