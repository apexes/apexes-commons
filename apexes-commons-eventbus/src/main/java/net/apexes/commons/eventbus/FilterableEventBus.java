/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;


/**
 * 具备过滤功能的EventBus
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class FilterableEventBus {
    
    private final EventBus eventBus = new EventBus();
    
    private final EventBusFilter eventBusFilter = new EventBusFilter(new IPublisher()  {
        @Override
        public <T> void publish(String address, T event) {
            eventBus.post(address, event);
        }
    });
    
    public void register(Object object) {
        eventBusFilter.register(object);
        eventBus.register(object);
    }

    public <T> void register(String address, IEventHandler<T> handler) {
        eventBusFilter.register(address, handler);
        eventBus.register(address, handler);
    }
    
    public void unregister(Object object) {
        eventBus.unregister(object);
        eventBusFilter.unregister(object);
    }

    public <T> void unregister(String address, IEventHandler<T> handler) {
        eventBus.unregister(address, handler);
        eventBusFilter.unregister(address, handler);
    }
    
    public void post(Object event) {
        eventBusFilter.post(event);
    }

    public <E> void post(String address, E event) {
        eventBusFilter.post(address, event);
    }
    
}
