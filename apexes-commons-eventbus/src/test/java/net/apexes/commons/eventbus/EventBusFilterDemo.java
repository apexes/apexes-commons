/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;


/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class EventBusFilterDemo {
    
    public static void main(String[] args) {
        EventBusFilter busFilter = new EventBusFilter(new SimplePostPublisher()); 
        Filter1 filter1 = new Filter1();
        Filter2 filter2 = new Filter2();
        Filter3 filter3 = new Filter3();
        System.out.println("address = " + EventBusFilter.getAddress(filter3, IEventFilter.class));
        busFilter.register(filter1);
        busFilter.register(filter2);
        busFilter.register(filter3);
        
        Test1 test1 = new Test1();
        Test2 test2 = new Test2();
        busFilter.register(test1);
        busFilter.register(test2);
        
        System.out.println("$$ Integer");
        busFilter.post(13);
        System.out.println("$$ Object");
        busFilter.post(new Object());
        System.out.println("$$ Long");
        busFilter.post(120L);
    }
    
    public static class Filter3 extends Filter1 {
        
    }
    
    public static class Filter1 implements IEventFilter<Integer> {
        
        @Override
        public void filter(Integer event, EventFilterChain<Integer> chain) {
            System.out.println("Filter1.filter Integer: " + event);
            //chain.next(event);
        }

        @Override
        public int getPriority() {
            return 0;
        }
        
    }
    
    @SuppressWarnings("rawtypes")
    public static class Filter2 implements IEventFilter {
        
        @SuppressWarnings("unchecked")
        @Override
        public void filter(Object event, EventFilterChain chain) {
            System.out.println("Filter2.filter Object: " + event);
            chain.next(event);
        }

        @Override
        public int getPriority() {
            return 0;
        }
        
    }
    
    public static class Test1 {
        
        @EventFilter
        public void onFilter(Integer event, EventFilterChain<Integer> chain) {
            System.out.println("Test1.filter Integer: " + event);
            chain.next(event);
        }
        
        @EventFilter(priority = 2)
        public void onFilter(Long event, EventFilterChain<Long> chain) {
            System.out.println("Test1.filter Long: " + event);
            chain.next(event);
        }
        
        @EventFilter(priority = 9)
        public void filter(Object event, EventFilterChain<Object> chain) {
            System.out.println("Test1.filter Object: " + event);
            chain.next(event);
        }
    }
    
    public static class Test2 {
        
        @EventFilter(priority = 9)
        public void onFilter(Integer event, EventFilterChain<Integer> chain) {
            System.out.println("Test2.filter Integer: " + event);
            chain.next(event);
            //chain.doFilter(event);
        }
        
        @EventFilter
        public void onFilter(Long event, EventFilterChain<Long> chain) {
            System.out.println("Test2.filter Long: " + event);
            chain.next(event);
        }
        
        @EventFilter
        public void onFilter(Object event, EventFilterChain<Object> chain) {
            System.out.println("Test2.filter Object: " + event);
            chain.next(event);
        }
    }
    
    private static class SimplePostPublisher implements IPublisher {

        @Override
        public <T> void publish(String address, T event) {
            System.out.println("### publish: " 
                    + event.getClass().getSimpleName()
                    + ": " +  event);
        }
        
    }
    
}
