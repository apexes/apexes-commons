/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class FilterableEventBusDemo {
    
    public static void main(String[] args) {
        FilterableEventBus eventBus = new FilterableEventBus();
        eventBus.register(new Test1());
        eventBus.register(new Test2());
        
        eventBus.post(Integer.valueOf(12));
        eventBus.post(Double.valueOf(11.5));
    }
    
    public static class Test1 {
        
        @Subscribe(priority = 8)
        public void onEvent(Integer event) {
            System.out.println("Test1.Integer: " + event);
        }
        
        @EventFilter(priority = 2)
        public void onFilter(Integer event, EventFilterChain<Integer> chain) {
            System.out.println("Test1.filter.Integer: " + event);
            chain.next(event + 2);
        }
        
        @Subscribe(priority = 3)
        public void onEvent(Double event) {
            System.out.println("Test1.Double: " + event);
        }
        
        @EventFilter(priority = 5)
        public void onFilter(Double event, EventFilterChain<Double> chain) {
            System.out.println("Test1.filter.Double: " + event);
            chain.next(event);
        }
    }
    
    public static class Test2 {
        
        @Subscribe(priority = 5)
        public void onEvent(Integer event) {
            System.out.println("Test2.Integer: " + event);
        }
        
        @EventFilter(priority = 7)
        public void onFilter(Integer event, EventFilterChain<Integer> chain) {
            System.out.println("Test2.filter.Integer: " + event);
            chain.next(event);
        }
        
        @Subscribe(priority = 9)
        public void onEvent(Double event) {
            System.out.println("Test2.Double: " + event);
        }
        
        @EventFilter(priority = 4)
        public void onFilter(Double event, EventFilterChain<Double> chain) {
            System.out.println("Test2.filter.Double: " + event);
//            chain.next(event);
        }
        
    }

}
