/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.commons.eventbus;


/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class EventBusDemo {
    
    public static void main(String[] args) {
        EventBus eventBus = new EventBus();
        eventBus.register(new Test1());
        eventBus.register(new Test2());
        eventBus.register(new Test3());
        
        eventBus.post(Integer.valueOf(10));
        eventBus.post(new Object());
    }
    
    public static class Test1 {
        
        @Subscribe(priority = 8)
        public void onEvent(Integer event) {
            System.out.println("Test1.Integer: " + event);
        }
        
        @Subscribe(priority = 2)
        public void onEvent(Object event) {
            System.out.println("Test1.Object: " + event);
        }
        
    }
    
    public static class Test2 {
        
        @Subscribe(priority = 5)
        public void onEvent(Integer event) {
            System.out.println("Test2.Integer: " + event);
        }
        
        @Subscribe(priority = 8)
        public void onEvent(Object event) {
            System.out.println("Test2.Object: " + event);
        }
    }
    
    public static class Test3 {
        
        @Subscribe(priority = 7)
        public void onEvent(Integer event) {
            System.out.println("Test3.Integer: " + event);
//            chain.next(event);
        }
        
        @Subscribe(priority = 2)
        public void onEvent(Object event) {
            System.out.println("Test3.Object: " + event);
//            chain.next(event);
        }
    }

}
